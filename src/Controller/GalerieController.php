<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class GalerieController extends AbstractController
{
    public function index()
    {
    unset($images1);
    $folder1 = opendir("assets/img/categorie1/");
    $i = 0;
    
    while(false !=($file1 = readdir($folder1))){
        if($file1 != "." && $file1 != ".."){
            $images1[$i]= $file1;
            $i++;
        }
        
    }
    $random_img1=$images1[rand(0,count($images1)-1)];

    unset($images2);
    $folder2 = opendir("assets/img/categorie2/");
    $i = 0;
    
    while(false !=($file2 = readdir($folder2))){
        if($file2 != "." && $file2 != ".."){
            $images2[$i]= $file2;
            $i++;
        }
        
    }
    $random_img2=$images2[rand(0,count($images2)-1)];

    unset($images3);
    $folder3 = opendir("assets/img/categorie3/");
    $i = 0;
    
    while(false !=($file3 = readdir($folder3))){
        if($file3 != "." && $file3 != ".."){
            $images3[$i]= $file3;
            $i++;
        }
        
    }
    $random_img3=$images3[rand(0,count($images3)-1)];
    var_dump($random_img1);
    var_dump($random_img2);
    var_dump($random_img3);
        return $this->render('photobook/photobook.html.twig', [
            'controller_name' => 'GalerieController',
            'img1' => $random_img1,
            'img2' => $random_img2,
            'img3' => $random_img3,
        ]);
    }
    

    public function categorie1()
    {
        return $this->render('photobook/categorie1.html.twig', [
            'controller_name' => 'GalerieController',
        ]);
    }

    public function categorie2()
    {
        return $this->render('photobook/categorie2.html.twig', [
            'controller_name' => 'GalerieController',
        ]);
    }

    public function categorie3()
    {
        return $this->render('photobook/categorie3.html.twig', [
            'controller_name' => 'GalerieController',
        ]);
    }

    public function categorie4()
    {
        return $this->render('photobook/categorie4.html.twig', [
            'controller_name' => 'GalerieController',
        ]);
    }
}
