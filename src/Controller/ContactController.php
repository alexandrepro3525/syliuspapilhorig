<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use \Swift_SmtpTransport,\Swift_Mailer,\Swift_Message;

use App\Form\ContactType;
use App\Entity\Contact;
use App\Repository\ContactRepository;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_create")
     */

    public function form(Contact $contact = null , Request $request, \Swift_Mailer $mailer) {

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData('contact');
            $this->sendMail($contact, $mailer);

            return $this->redirectToRoute('contact_create');
        }
        
        return $this->render('contact/index.html.twig', [
            'formContact' => $form->createView()
        ]);
    }

    public function sendMail(contact $contact, \Swift_Mailer $mailer)
    {
        require_once 'D:/Formation/PAPILHORIG/vendor/swiftmailer/swiftmailer/lib/swift_required.php';
        require_once 'D:/Formation/PAPILHORIG/vendor/autoload.php';
        
        
        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465,'ssl'))
            ->setUsername('xxalexxxx35@gmail.com')->setPassword('*');

        $mailer = new Swift_Mailer($transport);

        $message = (new Swift_Message('Our Code World Newsletter'))
        ->setFrom(array('xxalexxxx35@gmail.com' => 'Our Code World'))
        ->setTo(array("alexandre.pro3525@gmail.com" => "mail@mail.com"))
        ->setBody("<h1>Welcome</h1>", 'text/html');

        $result = $mailer->send($message);

    }
}
