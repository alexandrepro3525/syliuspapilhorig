<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200210153531 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE commentaires CHANGE validation validation TINYINT(1) DEFAULT \'false\'');
        $this->addSql('ALTER TABLE contact CHANGE phone_number phone_number VARCHAR(10) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP description');
        $this->addSql('ALTER TABLE commentaires CHANGE validation validation TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE contact CHANGE phone_number phone_number INT NOT NULL');
    }
}
