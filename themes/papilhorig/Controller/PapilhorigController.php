<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Themes\papilhorig\Entity\Article;
use App\Themes\papilhorig\Entity\Commentaires;
use App\Themes\papilhorig\Repository\ArticleRepository;
use App\Themes\papilhorig\Repository\CommentairesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Themes\papilhorig\Form\ArticleType;
use App\Themes\papilhorig\Form\CommentaireType;

class PapilhorigController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
        {
            return $this->render('papilhorig/index.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/pressBook/affichage", name="PressBookAffichage")
     */
    public function pressBookAffichage(ArticleRepository $repository)
    {
        $repository = $this -> getDoctrine()->getRepository(Article::class);

        $articles = $repository->findAll();
        return $this->render(
                'papilhorig/pressBook/pressBookAffichage.html.twig', 
                [
                    'articles' => $articles
                ]
        );
    }
        
    /**
     * @Route("/pressBook/Affichage/{id}", name="PressBookDetail")
     */
    public function pressBookDetail(CommentairesRepository $repoCommentaires, ArticleRepository $repoArticle, $id, EntityManagerInterface $em, Request $request){
        
        $commentaires = new Commentaires();

        $repoArticle = $this->getDoctrine()->getRepository(Article::class);
        $article = $repoArticle->find($id);

        $repoCommentaires = $this->getDoctrine()->getRepository(Commentaires::class);
        $listeCommentaires = $repoCommentaires->findAll();

        $formCommentaire = $this->createForm(CommentaireType::class, $commentaires);

        $formCommentaire->handlerequest($request);
        dump($commentaires);

        if($formCommentaire->isSubmitted() && $formCommentaire->isValid()) {
            $commentaires->setCreatedAt(new \DateTime());
            $commentaires->setArticle($article);
            $commentaires->setValidation((bool) false);
            $em->persist($commentaires);
            $em->flush();
            return $this->redirectToRoute('PressBookDetail', ['id' => $article->getId(),
            'listeCommentaires' => $listeCommentaires]);
        }

        return $this->render(
                        'papilhorig/pressBook/pressBookDetail.html.twig',
                        [
                            'formCommentaire' => $formCommentaire->createView(),
                            'article' => $article,
                            'listeCommentaires' => $listeCommentaires
                        ]
                    );
    }

    /**
     * @Route("/pressBook/new", name="PressBookCreate")
     */
    public function pressBookCreate(EntityManagerInterface $em, Request $request) {
            
        $article = new Article();
          
        $form = $this->createForm(ArticleType::class, $article);

        $form->handlerequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $article->setCreatedAt(new \DateTime());
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute('PressBookDetail', ['id' => $article->getId()]);
        }
        return $this->render('papilhorig/pressBook/pressBookCreate.html.twig', 
                            [
                                'formArticle' => $form->createView(),
                                'article' => $article
                            ]
                        );
        
    }

    /**
     * @Route("/pressBook/{id}/edit", name="PressBookEdit")
     */
    public function pressBookUpdate($id, ArticleRepository $repository, EntityManagerInterface $em, Request $request) {

        $repository = $this->getDoctrine()->getRepository(Article::class);

        $article = $repository->find($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handlerequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute('PressBookDetail', ['id' => $article->getId()]);
        }
        return $this->render('papilhorig/pressBook/pressBookCreate.html.twig', 
                            [
                                'formArticle' => $form->createView(),
                                'editMode' => $article->getId() !== null,
                                'article' => $article
                            ]
                        );
        
    }

    /**
     * @Route("/produits", name="Produits")
     */
    public function produits()
        {
            return $this->render('papilhorig/produits.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }
    
    /**
     * @Route("/photobook", name="Photobook")
     */
    public function photobook()
        {
            return $this->render('papilhorig/photobook/photobook.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/creation", name="Creation")
     */
    public function creation()
        {
            return $this->render('papilhorig/creation.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }
    /**
     * @Route("/contact", name="Contact")
     */
    public function contact()
        {
            return $this->render('papilhorig/contact.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/presentation", name="Presentation")
     */
    public function presentation()
        {
            return $this->render('papilhorig/presentation.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/photobook/categorie1", name="categorie1")
     */
    public function categorie1()
        {
            return $this->render('papilhorig/photobook/categorie1.html.twig');
        }

    /**
     * @Route("/photobook/categorie2", name="categorie2")
     */
    public function categorie2()
        {
            return $this->render('papilhorig/photobook/categorie2.html.twig');
        }

    /**
     * @Route("/photobook/categorie3", name="categorie3")
     */
    public function categorie3()
        {
            return $this->render('papilhorig/photobook/categorie3.html.twig');
        }

    /**
     * @Route("/photobook/categorie4", name="categorie4")
     */
    public function categorie4()
        {
            return $this->render('papilhorig/photobook/categorie4.html.twig');
        }
}
