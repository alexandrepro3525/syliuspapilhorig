<?php
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\sylius\\src\\Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\PriceHelper.php';

class PriceHelper_2012e62 extends \Sylius\Bundle\CoreBundle\Templating\Helper\PriceHelper implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder54247 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2b541 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties9c543 = [
        
    ];

    public function getPrice(\Sylius\Component\Core\Model\ProductVariantInterface $productVariant, array $context) : int
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getPrice', array('productVariant' => $productVariant, 'context' => $context), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getPrice($productVariant, $context);
    }

    public function getName() : string
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getName', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getName();
    }

    public function setCharset($charset)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'setCharset', array('charset' => $charset), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->setCharset($charset);
    }

    public function getCharset()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getCharset', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getCharset();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        unset($instance->charset);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\Templating\Helper\PriceHelper $instance) {
            unset($instance->productVariantPriceCalculator);
        }, $instance, 'Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\PriceHelper')->__invoke($instance);

        $instance->initializer2b541 = $initializer;

        return $instance;
    }

    public function __construct(\Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface $productVariantPriceCalculator)
    {
        static $reflection;

        if (! $this->valueHolder54247) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\PriceHelper');
            $this->valueHolder54247 = $reflection->newInstanceWithoutConstructor();
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\Templating\Helper\PriceHelper $instance) {
            unset($instance->productVariantPriceCalculator);
        }, $this, 'Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\PriceHelper')->__invoke($this);

        }

        $this->valueHolder54247->__construct($productVariantPriceCalculator);
    }

    public function & __get($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__get', ['name' => $name], $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        if (isset(self::$publicProperties9c543[$name])) {
            return $this->valueHolder54247->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__isset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__unset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__clone', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $this->valueHolder54247 = clone $this->valueHolder54247;
    }

    public function __sleep()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__sleep', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return array('valueHolder54247');
    }

    public function __wakeup()
    {
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\Templating\Helper\PriceHelper $instance) {
            unset($instance->productVariantPriceCalculator);
        }, $this, 'Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\PriceHelper')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer2b541 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer2b541;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'initializeProxy', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder54247;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder54247;
    }


}
