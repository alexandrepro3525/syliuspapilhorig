<?php
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\grid-bundle\\src\\Bundle\\Templating\\Helper\\GridHelper.php';

class GridHelper_dc9cf99 extends \Sylius\Bundle\GridBundle\Templating\Helper\GridHelper implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder54247 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2b541 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties9c543 = [
        
    ];

    public function renderGrid(\Sylius\Component\Grid\View\GridView $gridView, ?string $template = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'renderGrid', array('gridView' => $gridView, 'template' => $template), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->renderGrid($gridView, $template);
    }

    public function renderField(\Sylius\Component\Grid\View\GridView $gridView, \Sylius\Component\Grid\Definition\Field $field, $data)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'renderField', array('gridView' => $gridView, 'field' => $field, 'data' => $data), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->renderField($gridView, $field, $data);
    }

    public function renderAction(\Sylius\Component\Grid\View\GridView $gridView, \Sylius\Component\Grid\Definition\Action $action, $data = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'renderAction', array('gridView' => $gridView, 'action' => $action, 'data' => $data), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->renderAction($gridView, $action, $data);
    }

    public function renderFilter(\Sylius\Component\Grid\View\GridView $gridView, \Sylius\Component\Grid\Definition\Filter $filter)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'renderFilter', array('gridView' => $gridView, 'filter' => $filter), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->renderFilter($gridView, $filter);
    }

    public function getName() : string
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getName', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getName();
    }

    public function setCharset($charset)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'setCharset', array('charset' => $charset), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->setCharset($charset);
    }

    public function getCharset()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getCharset', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getCharset();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        unset($instance->charset);

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\GridHelper $instance) {
            unset($instance->gridRenderer);
        }, $instance, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\GridHelper')->__invoke($instance);

        $instance->initializer2b541 = $initializer;

        return $instance;
    }

    public function __construct(\Sylius\Component\Grid\Renderer\GridRendererInterface $gridRenderer)
    {
        static $reflection;

        if (! $this->valueHolder54247) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\GridHelper');
            $this->valueHolder54247 = $reflection->newInstanceWithoutConstructor();
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\GridHelper $instance) {
            unset($instance->gridRenderer);
        }, $this, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\GridHelper')->__invoke($this);

        }

        $this->valueHolder54247->__construct($gridRenderer);
    }

    public function & __get($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__get', ['name' => $name], $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        if (isset(self::$publicProperties9c543[$name])) {
            return $this->valueHolder54247->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__isset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__unset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__clone', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $this->valueHolder54247 = clone $this->valueHolder54247;
    }

    public function __sleep()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__sleep', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return array('valueHolder54247');
    }

    public function __wakeup()
    {
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\GridHelper $instance) {
            unset($instance->gridRenderer);
        }, $this, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\GridHelper')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer2b541 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer2b541;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'initializeProxy', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder54247;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder54247;
    }


}
