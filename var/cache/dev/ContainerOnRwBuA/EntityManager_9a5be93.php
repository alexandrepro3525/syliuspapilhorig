<?php
include_once \dirname(__DIR__, 4).'\\vendor\\doctrine\\persistence\\lib\\Doctrine\\Common\\Persistence\\ObjectManager.php';
include_once \dirname(__DIR__, 4).'\\vendor\\doctrine\\orm\\lib\\Doctrine\\ORM\\EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\doctrine\\orm\\lib\\Doctrine\\ORM\\EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder54247 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2b541 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties9c543 = [
        
    ];

    public function getConnection()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getConnection', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getMetadataFactory', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getExpressionBuilder', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'beginTransaction', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getCache', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getCache();
    }

    public function transactional($func)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'transactional', array('func' => $func), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->transactional($func);
    }

    public function commit()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'commit', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->commit();
    }

    public function rollback()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'rollback', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getClassMetadata', array('className' => $className), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'createQuery', array('dql' => $dql), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'createNamedQuery', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'createQueryBuilder', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'flush', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->flush($entity);
    }

    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->find($entityName, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'clear', array('entityName' => $entityName), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->clear($entityName);
    }

    public function close()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'close', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->close();
    }

    public function persist($entity)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'persist', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'remove', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'refresh', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'detach', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'merge', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getRepository', array('entityName' => $entityName), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'contains', array('entity' => $entity), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getEventManager', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getConfiguration', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'isOpen', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getUnitOfWork', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getProxyFactory', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'initializeObject', array('obj' => $obj), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getFilters', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'isFiltersStateClean', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'hasFilters', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer2b541 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder54247) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder54247 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder54247->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__get', ['name' => $name], $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        if (isset(self::$publicProperties9c543[$name])) {
            return $this->valueHolder54247->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__isset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__unset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__clone', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $this->valueHolder54247 = clone $this->valueHolder54247;
    }

    public function __sleep()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__sleep', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return array('valueHolder54247');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer2b541 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer2b541;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'initializeProxy', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder54247;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder54247;
    }


}
