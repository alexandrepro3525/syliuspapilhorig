<?php
include_once \dirname(__DIR__, 4).'\\vendor\\friendsofsymfony\\oauth2-php\\lib\\OAuth2.php';

class OAuth2_5885b82 extends \OAuth2\OAuth2 implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder54247 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2b541 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties9c543 = [
        
    ];

    public function getVariable($name, $default = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getVariable', array('name' => $name, 'default' => $default), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getVariable($name, $default);
    }

    public function setVariable($name, $value)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'setVariable', array('name' => $name, 'value' => $value), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->setVariable($name, $value);
    }

    public function verifyAccessToken($tokenParam, $scope = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'verifyAccessToken', array('tokenParam' => $tokenParam, 'scope' => $scope), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->verifyAccessToken($tokenParam, $scope);
    }

    public function getBearerToken(?\Symfony\Component\HttpFoundation\Request $request = null, $removeFromRequest = false)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'getBearerToken', array('request' => $request, 'removeFromRequest' => $removeFromRequest), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->getBearerToken($request, $removeFromRequest);
    }

    public function grantAccessToken(?\Symfony\Component\HttpFoundation\Request $request = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'grantAccessToken', array('request' => $request), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->grantAccessToken($request);
    }

    public function finishClientAuthorization($isAuthorized, $data = null, ?\Symfony\Component\HttpFoundation\Request $request = null, $scope = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'finishClientAuthorization', array('isAuthorized' => $isAuthorized, 'data' => $data, 'request' => $request, 'scope' => $scope), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->finishClientAuthorization($isAuthorized, $data, $request, $scope);
    }

    public function createAccessToken(\OAuth2\Model\IOAuth2Client $client, $data, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'createAccessToken', array('client' => $client, 'data' => $data, 'scope' => $scope, 'access_token_lifetime' => $access_token_lifetime, 'issue_refresh_token' => $issue_refresh_token, 'refresh_token_lifetime' => $refresh_token_lifetime), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return $this->valueHolder54247->createAccessToken($client, $data, $scope, $access_token_lifetime, $issue_refresh_token, $refresh_token_lifetime);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        unset($instance->conf, $instance->storage, $instance->oldRefreshToken, $instance->usedAuthCode);

        $instance->initializer2b541 = $initializer;

        return $instance;
    }

    public function __construct(\OAuth2\IOAuth2Storage $storage, $config = [])
    {
        static $reflection;

        if (! $this->valueHolder54247) {
            $reflection = $reflection ?? new \ReflectionClass('OAuth2\\OAuth2');
            $this->valueHolder54247 = $reflection->newInstanceWithoutConstructor();
        unset($this->conf, $this->storage, $this->oldRefreshToken, $this->usedAuthCode);

        }

        $this->valueHolder54247->__construct($storage, $config);
    }

    public function & __get($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__get', ['name' => $name], $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        if (isset(self::$publicProperties9c543[$name])) {
            return $this->valueHolder54247->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__isset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__unset', array('name' => $name), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder54247;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder54247;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__clone', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        $this->valueHolder54247 = clone $this->valueHolder54247;
    }

    public function __sleep()
    {
        $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, '__sleep', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;

        return array('valueHolder54247');
    }

    public function __wakeup()
    {
        unset($this->conf, $this->storage, $this->oldRefreshToken, $this->usedAuthCode);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer2b541 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer2b541;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2b541 && ($this->initializer2b541->__invoke($valueHolder54247, $this, 'initializeProxy', array(), $this->initializer2b541) || 1) && $this->valueHolder54247 = $valueHolder54247;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder54247;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder54247;
    }


}
