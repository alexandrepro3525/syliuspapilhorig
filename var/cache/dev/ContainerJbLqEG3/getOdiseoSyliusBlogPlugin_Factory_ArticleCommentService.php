<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'odiseo_sylius_blog_plugin.factory.article_comment' shared service.

include_once \dirname(__DIR__, 4).'\\vendor\\odiseoteam\\blog-bundle\\src\\Factory\\ArticleCommentFactoryInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\odiseoteam\\sylius-blog-plugin\\src\\Factory\\ArticleCommentFactory.php';
include_once \dirname(__DIR__, 4).'\\vendor\\odiseoteam\\blog-bundle\\src\\Factory\\ArticleCommentFactory.php';

return $this->privates['odiseo_sylius_blog_plugin.factory.article_comment'] = new \Odiseo\SyliusBlogPlugin\Factory\ArticleCommentFactory(new \Odiseo\BlogBundle\Factory\ArticleCommentFactory('Odiseo\\SyliusBlogPlugin\\Entity\\ArticleComment', ($this->services['odiseo_blog.repository.article'] ?? $this->load('getOdiseoBlog_Repository_ArticleService.php')), ($this->services['odiseo_blog.repository.article_comment'] ?? $this->load('getOdiseoBlog_Repository_ArticleCommentService.php'))), ($this->services['security.token_storage'] ?? $this->getSecurity_TokenStorageService()));
