<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Cart/Summary:_totals.html.twig */
class __TwigTemplate_fd7c6301ba97592653cf11306a1cb4ad42497cf0b8fd70dd91a3cf7ba6ea6ba7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Cart/Summary:_totals.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Cart/Summary:_totals.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Cart/Summary:_totals.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["itemsSubtotal"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderItemsSubtotalExtension']->getSubtotal((isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 3, $this->source); })()));
        // line 4
        $context["taxIncluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getIncludedTax((isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 4, $this->source); })()));
        // line 5
        $context["taxExcluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getExcludedTax((isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 5, $this->source); })()));
        // line 6
        echo "
<div class=\"ui segment\">
    <h2 class=\"ui dividing header\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.summary"), "html", null, true);
        echo "</h2>

    ";
        // line 10
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.totals", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 10, $this->source); })())]]);
        echo "

    <table class=\"ui very basic table\">
        <tbody>
        <tr>
            <td>";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.items_total"), "html", null, true);
        echo ":</td>
            <td class=\"right aligned\">";
        // line 16
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["itemsSubtotal"]) || array_key_exists("itemsSubtotal", $context) ? $context["itemsSubtotal"] : (function () { throw new RuntimeError('Variable "itemsSubtotal" does not exist.', 16, $this->source); })())], 16, $context, $this->getSourceContext());
        echo "</td>
        </tr>
        ";
        // line 18
        if (twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 18, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 18)) {
            // line 19
            echo "        <tr>
            <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.discount"), "html", null, true);
            echo ":</td>
            <td id=\"sylius-cart-promotion-total\" class=\"right aligned\">";
            // line 21
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 21, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 21)], 21, $context, $this->getSourceContext());
            echo "</td>
        </tr>
        ";
        }
        // line 24
        echo "        <tr>
            <td>";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_estimated_cost"), "html", null, true);
        echo ":</td>
            <td class=\"right aligned\">
                ";
        // line 27
        if ((twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 27, $this->source); })()), "getAdjustmentsTotal", [0 => "shipping"], "method", false, false, false, 27) > twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 27, $this->source); })()), "shippingTotal", [], "any", false, false, false, 27))) {
            // line 28
            echo "                    <span class=\"old-price\">";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 28, $this->source); })()), "getAdjustmentsTotal", [0 => "shipping"], "method", false, false, false, 28)], 28, $context, $this->getSourceContext());
            echo "</span>
                ";
        }
        // line 30
        echo "                <span id=\"sylius-cart-shipping-total\">";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 30, $this->source); })()), "shippingTotal", [], "any", false, false, false, 30)], 30, $context, $this->getSourceContext());
        echo "</span>
            </td>
        </tr>
        <tr ";
        // line 33
        if (((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 33, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 33, $this->source); })()))) {
            echo "class=\"tax-disabled\"";
        }
        echo ">
            <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.taxes_total"), "html", null, true);
        echo ":</td>
            <td class=\"right aligned\">
                ";
        // line 36
        if (( !(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 36, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 36, $this->source); })()))) {
            // line 37
            echo "                    <div id=\"sylius-cart-tax-none\">";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [0], 37, $context, $this->getSourceContext());
            echo "</div>
                ";
        }
        // line 39
        echo "                ";
        if ((isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 39, $this->source); })())) {
            // line 40
            echo "                    <div id=\"sylius-cart-tax-excluded\">";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 40, $this->source); })())], 40, $context, $this->getSourceContext());
            echo "</div>
                ";
        }
        // line 42
        echo "                ";
        if ((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 42, $this->source); })())) {
            // line 43
            echo "                    <div class=\"tax-disabled\">
                        <small>(";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.included_in_price"), "html", null, true);
            echo ")</small>
                        <span id=\"sylius-cart-tax-included\">";
            // line 45
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 45, $this->source); })())], 45, $context, $this->getSourceContext());
            echo "</span>
                    </div>
                ";
        }
        // line 48
        echo "            </td>
        </tr>
        <tr class=\"ui large header\">
            <td>";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.order_total"), "html", null, true);
        echo ":</td>
            <td id=\"sylius-cart-grand-total\" class=\"right aligned\">";
        // line 52
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 52, $this->source); })()), "total", [], "any", false, false, false, 52)], 52, $context, $this->getSourceContext());
        echo "</td>
        </tr>
        ";
        // line 54
        if ( !(twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 54, $this->source); })()), "currencyCode", [], "any", false, false, false, 54) === twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 54, $this->source); })()), "currencyCode", [], "any", false, false, false, 54))) {
            // line 55
            echo "            <tr>
                <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.base_currency_order_total"), "html", null, true);
            echo ":</td>
                <td id=\"sylius-cart-base-grand-total\" class=\"right aligned\">";
            // line 57
            echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 57, $this->source); })()), "total", [], "any", false, false, false, 57), twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 57, $this->source); })()), "currencyCode", [], "any", false, false, false, 57)], 57, $context, $this->getSourceContext());
            echo "</td>
            </tr>
        ";
        }
        // line 60
        echo "        </tbody>
    </table>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Cart/Summary:_totals.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 60,  185 => 57,  181 => 56,  178 => 55,  176 => 54,  171 => 52,  167 => 51,  162 => 48,  156 => 45,  152 => 44,  149 => 43,  146 => 42,  140 => 40,  137 => 39,  131 => 37,  129 => 36,  124 => 34,  118 => 33,  111 => 30,  105 => 28,  103 => 27,  98 => 25,  95 => 24,  89 => 21,  85 => 20,  82 => 19,  80 => 18,  75 => 16,  71 => 15,  63 => 10,  58 => 8,  54 => 6,  52 => 5,  50 => 4,  48 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}

{% set itemsSubtotal = sylius_order_items_subtotal(cart) %}
{% set taxIncluded = sylius_order_tax_included(cart) %}
{% set taxExcluded = sylius_order_tax_excluded(cart) %}

<div class=\"ui segment\">
    <h2 class=\"ui dividing header\">{{ 'sylius.ui.summary'|trans }}</h2>

    {{ sonata_block_render_event('sylius.shop.cart.summary.totals', {'cart': cart}) }}

    <table class=\"ui very basic table\">
        <tbody>
        <tr>
            <td>{{ 'sylius.ui.items_total'|trans }}:</td>
            <td class=\"right aligned\">{{ money.convertAndFormat(itemsSubtotal) }}</td>
        </tr>
        {% if cart.orderPromotionTotal %}
        <tr>
            <td>{{ 'sylius.ui.discount'|trans }}:</td>
            <td id=\"sylius-cart-promotion-total\" class=\"right aligned\">{{ money.convertAndFormat(cart.orderPromotionTotal) }}</td>
        </tr>
        {% endif %}
        <tr>
            <td>{{ 'sylius.ui.shipping_estimated_cost'|trans }}:</td>
            <td class=\"right aligned\">
                {% if cart.getAdjustmentsTotal('shipping') > cart.shippingTotal %}
                    <span class=\"old-price\">{{ money.convertAndFormat(cart.getAdjustmentsTotal('shipping')) }}</span>
                {% endif %}
                <span id=\"sylius-cart-shipping-total\">{{ money.convertAndFormat(cart.shippingTotal) }}</span>
            </td>
        </tr>
        <tr {% if taxIncluded and not taxExcluded %}class=\"tax-disabled\"{% endif %}>
            <td>{{ 'sylius.ui.taxes_total'|trans }}:</td>
            <td class=\"right aligned\">
                {% if not taxIncluded and not taxExcluded %}
                    <div id=\"sylius-cart-tax-none\">{{ money.convertAndFormat(0) }}</div>
                {% endif %}
                {% if taxExcluded %}
                    <div id=\"sylius-cart-tax-excluded\">{{ money.convertAndFormat(taxExcluded) }}</div>
                {% endif %}
                {% if taxIncluded %}
                    <div class=\"tax-disabled\">
                        <small>({{ 'sylius.ui.included_in_price'|trans }})</small>
                        <span id=\"sylius-cart-tax-included\">{{ money.convertAndFormat(taxIncluded) }}</span>
                    </div>
                {% endif %}
            </td>
        </tr>
        <tr class=\"ui large header\">
            <td>{{ 'sylius.ui.order_total'|trans }}:</td>
            <td id=\"sylius-cart-grand-total\" class=\"right aligned\">{{ money.convertAndFormat(cart.total) }}</td>
        </tr>
        {% if cart.currencyCode is not same as(sylius.currencyCode) %}
            <tr>
                <td>{{ 'sylius.ui.base_currency_order_total'|trans }}:</td>
                <td id=\"sylius-cart-base-grand-total\" class=\"right aligned\">{{ money.format(cart.total, cart.currencyCode) }}</td>
            </tr>
        {% endif %}
        </tbody>
    </table>
</div>
", "SyliusShopBundle:Cart/Summary:_totals.html.twig", "D:\\Formation\\Sylius\\vendor\\sylius\\sylius\\src\\Sylius\\Bundle\\ShopBundle\\Resources\\views\\Cart\\Summary\\_totals.html.twig");
    }
}
