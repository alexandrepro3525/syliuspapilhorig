<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig */
class __TwigTemplate_feedac75654a78bf8502ea7c4308aab8492f117aaa6e2a9bd5d4987661e92b62 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'blog_breadcrumb' => [$this, 'block_blog_breadcrumb'],
            'blog_header' => [$this, 'block_blog_header'],
            'blog_articles' => [$this, 'block_blog_articles'],
            'blog_disqus' => [$this, 'block_blog_disqus'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 4
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig"));

        // line 1
        $macros["pagination"] = $this->macros["pagination"] = $this->loadTemplate("@SyliusUi/Macro/pagination.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig", 1)->unwrap();
        // line 2
        $macros["messages"] = $this->macros["messages"] = $this->loadTemplate("SyliusUiBundle:Macro:messages.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig", 2)->unwrap();
        // line 4
        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig", 4);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    ";
        $this->displayBlock('blog_breadcrumb', $context, $blocks);
        // line 14
        echo "
    ";
        // line 15
        $this->displayBlock('blog_header', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('blog_articles', $context, $blocks);
        // line 87
        echo "
    ";
        // line 88
        if ($this->extensions['Odiseo\SyliusBlogPlugin\Twig\BlogExtension']->getDisqusShortname()) {
            // line 89
            echo "        ";
            $this->displayBlock('blog_disqus', $context, $blocks);
            // line 92
            echo "    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_blog_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_breadcrumb"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_breadcrumb"));

        // line 8
        echo "        <div class=\"ui breadcrumb\">
            <a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_homepage");
        echo "\" class=\"section\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.home"), "html", null, true);
        echo "</a>
            <div class=\"divider\"> / </div>
            <div class=\"active section\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.blog.header"), "html", null, true);
        echo "</div>
        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_blog_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_header"));

        // line 16
        echo "        <h1 class=\"ui header\">
            <i class=\"circular newspaper icon\"></i>
            <div class=\"content\">
                ";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.blog.header"), "html", null, true);
        echo "
                <div class=\"sub header\">";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.blog.subheader"), "html", null, true);
        echo "</div>
            </div>
        </h1>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_blog_articles($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_articles"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_articles"));

        // line 26
        echo "        <div class=\"ui grid\">
            <div class=\"sixteen wide mobile twelve wide computer column\">
                <div class=\"ui segment\">
                    ";
        // line 29
        if ((twig_length_filter($this->env, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 29, $this->source); })())) > 0)) {
            // line 30
            echo "                        <div class=\"ui divided items\">
                            ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 31, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
                // line 32
                echo "                                <div class=\"item\">
                                    ";
                // line 33
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "images", [], "any", false, false, false, 33)) > 0)) {
                    // line 34
                    echo "                                        <div class=\"image\">
                                            <img src=\"";
                    // line 35
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["article"], "images", [], "any", false, false, false, 35), 0, [], "array", false, false, false, 35), "path", [], "any", false, false, false, 35), (((isset($context["filter"]) || array_key_exists("filter", $context))) ? (_twig_default_filter((isset($context["filter"]) || array_key_exists("filter", $context) ? $context["filter"] : (function () { throw new RuntimeError('Variable "filter" does not exist.', 35, $this->source); })()), "sylius_small")) : ("sylius_small"))), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["article"], "images", [], "any", false, false, false, 35), 0, [], "array", false, false, false, 35), "path", [], "any", false, false, false, 35), "html", null, true);
                    echo "\">
                                        </div>
                                    ";
                }
                // line 38
                echo "                                    <div class=\"content\">
                                        <a class=\"header\" href=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_show", ["slug" => twig_get_attribute($this->env, $this->source, $context["article"], "slug", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\">
                                            ";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "title", [], "any", false, false, false, 40), "html", null, true);
                echo "
                                        </a>
                                        <br>
                                        <div class=\"ui aligned grid\">
                                            <div class=\"left aligned eight wide column\">
                                                <span>";
                // line 45
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "createdAt", [], "any", false, false, false, 45)), "html", null, true);
                echo "</span>
                                                ";
                // line 46
                if (twig_get_attribute($this->env, $this->source, $context["article"], "author", [], "any", false, false, false, 46)) {
                    // line 47
                    echo "                                                    <span>by <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_index_by_author", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["article"], "author", [], "any", false, false, false, 47), "username", [], "any", false, false, false, 47)]), "html", null, true);
                    echo "\"><strong>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["article"], "author", [], "any", false, false, false, 47), "username", [], "any", false, false, false, 47), "html", null, true);
                    echo "</strong></a> </span>
                                                ";
                }
                // line 49
                echo "                                            </div>
                                            <div class=\"right aligned eight wide column\">
                                                ";
                // line 51
                if ($this->extensions['Odiseo\SyliusBlogPlugin\Twig\BlogExtension']->getDisqusShortname()) {
                    // line 52
                    echo "                                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_show", ["slug" => twig_get_attribute($this->env, $this->source, $context["article"], "slug", [], "any", false, false, false, 52)]), "html", null, true);
                    echo "#disqus_thread\" data-disqus-identifier=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("odiseo_sylius_blog_plugin_shop_article_show", ["slug" => twig_get_attribute($this->env, $this->source, $context["article"], "slug", [], "any", false, false, false, 52)]), "html", null, true);
                    echo "\">
                                                        <div class=\"ui active inline mini loader\"></div>
                                                    </a>
                                                ";
                } else {
                    // line 56
                    echo "                                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_show", ["slug" => twig_get_attribute($this->env, $this->source, $context["article"], "slug", [], "any", false, false, false, 56)]), "html", null, true);
                    echo "#blog-comments\">
                                                        <i class=\"ui comment icon\"></i> ";
                    // line 57
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "enabledComments", [], "any", false, false, false, 57)), "html", null, true);
                    echo " comments
                                                    </a>
                                                ";
                }
                // line 60
                echo "                                            </div>
                                        </div>
                                        <div class=\"description\">
                                            <p>";
                // line 63
                echo twig_escape_filter($this->env, (((twig_length_filter($this->env, strip_tags(twig_get_attribute($this->env, $this->source, $context["article"], "content", [], "any", false, false, false, 63))) > 200)) ? ((twig_slice($this->env, strip_tags(twig_get_attribute($this->env, $this->source, $context["article"], "content", [], "any", false, false, false, 63)), 0, 200) . "...")) : (strip_tags(twig_get_attribute($this->env, $this->source, $context["article"], "content", [], "any", false, false, false, 63)))), "html", null, true);
                echo "</p>
                                        </div>
                                        ";
                // line 65
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "categories", [], "any", false, false, false, 65)) > 0)) {
                    // line 66
                    echo "                                            <div class=\"extra\">
                                                ";
                    // line 67
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["article"], "categories", [], "any", false, false, false, 67));
                    foreach ($context['_seq'] as $context["_key"] => $context["articleCategory"]) {
                        // line 68
                        echo "                                                    <div class=\"ui label\"><a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_index_by_category", ["slug" => twig_get_attribute($this->env, $this->source, $context["articleCategory"], "slug", [], "any", false, false, false, 68)]), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articleCategory"], "title", [], "any", false, false, false, 68), "html", null, true);
                        echo "</a></div>
                                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['articleCategory'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 70
                    echo "                                            </div>
                                        ";
                }
                // line 72
                echo "                                    </div>
                                </div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "                        </div>
                        ";
            // line 76
            echo twig_call_macro($macros["pagination"], "macro_simple", [(isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 76, $this->source); })())], 76, $context, $this->getSourceContext());
            echo "
                    ";
        } else {
            // line 78
            echo "                        ";
            echo twig_call_macro($macros["messages"], "macro_info", ["sylius.ui.no_results_to_display"], 78, $context, $this->getSourceContext());
            echo "
                    ";
        }
        // line 80
        echo "                </div>
            </div>
            <div class=\"sixteen wide mobile four wide computer column\">
                ";
        // line 83
        $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/_sidebar.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig", 83)->display($context);
        // line 84
        echo "            </div>
        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 89
    public function block_blog_disqus($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_disqus"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_disqus"));

        // line 90
        echo "            ";
        $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/Article/_disqus.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig", 90)->display($context);
        // line 91
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 91,  356 => 90,  346 => 89,  334 => 84,  332 => 83,  327 => 80,  321 => 78,  316 => 76,  313 => 75,  305 => 72,  301 => 70,  290 => 68,  286 => 67,  283 => 66,  281 => 65,  276 => 63,  271 => 60,  265 => 57,  260 => 56,  250 => 52,  248 => 51,  244 => 49,  236 => 47,  234 => 46,  230 => 45,  222 => 40,  218 => 39,  215 => 38,  207 => 35,  204 => 34,  202 => 33,  199 => 32,  195 => 31,  192 => 30,  190 => 29,  185 => 26,  175 => 25,  161 => 20,  157 => 19,  152 => 16,  142 => 15,  129 => 11,  122 => 9,  119 => 8,  109 => 7,  98 => 92,  95 => 89,  93 => 88,  90 => 87,  88 => 25,  85 => 24,  83 => 15,  80 => 14,  77 => 7,  67 => 6,  56 => 4,  54 => 2,  52 => 1,  39 => 4,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusUi/Macro/pagination.html.twig' as pagination %}
{% import 'SyliusUiBundle:Macro:messages.html.twig' as messages %}

{% extends '@SyliusShop/layout.html.twig' %}

{% block content %}
    {% block blog_breadcrumb %}
        <div class=\"ui breadcrumb\">
            <a href=\"{{ path('sylius_shop_homepage') }}\" class=\"section\">{{ 'sylius.ui.home'|trans }}</a>
            <div class=\"divider\"> / </div>
            <div class=\"active section\">{{ 'odiseo_sylius_blog_plugin.ui.blog.header'|trans }}</div>
        </div>
    {% endblock %}

    {% block blog_header %}
        <h1 class=\"ui header\">
            <i class=\"circular newspaper icon\"></i>
            <div class=\"content\">
                {{ 'odiseo_sylius_blog_plugin.ui.blog.header'|trans }}
                <div class=\"sub header\">{{ 'odiseo_sylius_blog_plugin.ui.blog.subheader'|trans }}</div>
            </div>
        </h1>
    {% endblock %}

    {% block blog_articles %}
        <div class=\"ui grid\">
            <div class=\"sixteen wide mobile twelve wide computer column\">
                <div class=\"ui segment\">
                    {% if resources|length > 0 %}
                        <div class=\"ui divided items\">
                            {% for article in resources %}
                                <div class=\"item\">
                                    {% if article.images|length > 0 %}
                                        <div class=\"image\">
                                            <img src=\"{{ article.images[0].path|imagine_filter(filter|default('sylius_small')) }}\" alt=\"{{ article.images[0].path }}\">
                                        </div>
                                    {% endif %}
                                    <div class=\"content\">
                                        <a class=\"header\" href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_show', {'slug' : article.slug}) }}\">
                                            {{ article.title }}
                                        </a>
                                        <br>
                                        <div class=\"ui aligned grid\">
                                            <div class=\"left aligned eight wide column\">
                                                <span>{{ article.createdAt|date }}</span>
                                                {% if article.author %}
                                                    <span>by <a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_index_by_author', {'username': article.author.username}) }}\"><strong>{{ article.author.username }}</strong></a> </span>
                                                {% endif %}
                                            </div>
                                            <div class=\"right aligned eight wide column\">
                                                {% if disqus_shortname() %}
                                                    <a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_show', {'slug' : article.slug}) }}#disqus_thread\" data-disqus-identifier=\"{{ url('odiseo_sylius_blog_plugin_shop_article_show', {'slug' : article.slug}) }}\">
                                                        <div class=\"ui active inline mini loader\"></div>
                                                    </a>
                                                {% else %}
                                                    <a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_show', {'slug' : article.slug}) }}#blog-comments\">
                                                        <i class=\"ui comment icon\"></i> {{ article.enabledComments|length }} comments
                                                    </a>
                                                {% endif %}
                                            </div>
                                        </div>
                                        <div class=\"description\">
                                            <p>{{ article.content|striptags|length > 200 ? article.content|striptags|slice(0, 200) ~ '...' : article.content|striptags }}</p>
                                        </div>
                                        {% if article.categories|length > 0 %}
                                            <div class=\"extra\">
                                                {% for articleCategory in article.categories %}
                                                    <div class=\"ui label\"><a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_index_by_category', {'slug': articleCategory.slug}) }}\">{{ articleCategory.title }}</a></div>
                                                {% endfor %}
                                            </div>
                                        {% endif %}
                                    </div>
                                </div>
                            {% endfor %}
                        </div>
                        {{ pagination.simple(resources) }}
                    {% else %}
                        {{ messages.info('sylius.ui.no_results_to_display') }}
                    {% endif %}
                </div>
            </div>
            <div class=\"sixteen wide mobile four wide computer column\">
                {% include '@OdiseoSyliusBlogPlugin/Shop/_sidebar.html.twig' %}
            </div>
        </div>
    {% endblock %}

    {% if disqus_shortname() %}
        {% block blog_disqus %}
            {% include '@OdiseoSyliusBlogPlugin/Shop/Article/_disqus.html.twig' %}
        {% endblock %}
    {% endif %}
{% endblock %}
", "OdiseoSyliusBlogPlugin:Shop/Article:index.html.twig", "D:\\Formation\\Sylius\\vendor\\odiseoteam\\sylius-blog-plugin\\src/Resources/views/Shop/Article/index.html.twig");
    }
}
