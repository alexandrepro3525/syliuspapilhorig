<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OdiseoSyliusBlogPlugin:Shop/Article:_comment.html.twig */
class __TwigTemplate_47c104a36e4e1392655996d09f8a78b0bb2bed7905cdcdbc7459d704abd9ca31 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:_comment.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:_comment.html.twig"));

        // line 1
        echo "<div class=\"comment\">
    <a class=\"avatar\">
        <img src=\"//placehold.it/50x50\" alt=\"50x50\">
    </a>
    <div class=\"content\">
        <a class=\"author\">";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 6, $this->source); })()), "name", [], "any", false, false, false, 6), "html", null, true);
        echo "</a>
        <div class=\"metadata\">
            <span class=\"date\">";
        // line 8
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 8, $this->source); })()), "createdAt", [], "any", false, false, false, 8)), "html", null, true);
        echo "</span>
        </div>
        <div class=\"text\">
            ";
        // line 11
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 11, $this->source); })()), "comment", [], "any", false, false, false, 11), "html", null, true));
        echo "
        </div>
        ";
        // line 13
        if (((isset($context["withReply"]) || array_key_exists("withReply", $context)) && ((isset($context["withReply"]) || array_key_exists("withReply", $context) ? $context["withReply"] : (function () { throw new RuntimeError('Variable "withReply" does not exist.', 13, $this->source); })()) == true))) {
            // line 14
            echo "            <div class=\"actions\">
                <div class=\"ui accordion\">
                    <div class=\"title\">
                        ";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.reply"), "html", null, true);
            echo "
                    </div>
                    <div class=\"content\">
                        ";
            // line 20
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("odiseo_blog_partial_comment_create", ["articleId" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 21
(isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 21, $this->source); })()), "article", [], "any", false, false, false, 21), "id", [], "any", false, false, false, 21), "commentId" => twig_get_attribute($this->env, $this->source,             // line 22
(isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 22, $this->source); })()), "id", [], "any", false, false, false, 22), "template" => "@OdiseoSyliusBlogPlugin/Shop/Article/_comment_create.html.twig", "redirectRoute" => "odiseo_sylius_blog_plugin_shop_article_show", "redirectParameters" => ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 25
(isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 25, $this->source); })()), "article", [], "any", false, false, false, 25), "slug", [], "any", false, false, false, 25)]]));
            // line 26
            echo "
                    </div>
                </div>
            </div>
        ";
        }
        // line 31
        echo "    </div>
    ";
        // line 32
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 32, $this->source); })()), "enabledChildren", [], "any", false, false, false, 32)) > 0)) {
            // line 33
            echo "        <div class=\"comments\">
            ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["comment"], "enabledChildren", [], "any", false, false, false, 34));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                // line 35
                echo "                ";
                $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/Article/_comment.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:_comment.html.twig", 35)->display(twig_array_merge($context, ["comment" => $context["comment"], "withReply" => false]));
                // line 36
                echo "            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "        </div>
    ";
        }
        // line 39
        echo "</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OdiseoSyliusBlogPlugin:Shop/Article:_comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 39,  133 => 37,  119 => 36,  116 => 35,  99 => 34,  96 => 33,  94 => 32,  91 => 31,  84 => 26,  82 => 25,  81 => 22,  80 => 21,  79 => 20,  73 => 17,  68 => 14,  66 => 13,  61 => 11,  55 => 8,  50 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"comment\">
    <a class=\"avatar\">
        <img src=\"//placehold.it/50x50\" alt=\"50x50\">
    </a>
    <div class=\"content\">
        <a class=\"author\">{{ comment.name }}</a>
        <div class=\"metadata\">
            <span class=\"date\">{{ comment.createdAt|date }}</span>
        </div>
        <div class=\"text\">
            {{ comment.comment|nl2br }}
        </div>
        {% if withReply is defined and withReply == true %}
            <div class=\"actions\">
                <div class=\"ui accordion\">
                    <div class=\"title\">
                        {{ 'odiseo_sylius_blog_plugin.ui.reply'|trans }}
                    </div>
                    <div class=\"content\">
                        {{ render(url('odiseo_blog_partial_comment_create', {
                            'articleId': comment.article.id,
                            'commentId': comment.id,
                            'template': '@OdiseoSyliusBlogPlugin/Shop/Article/_comment_create.html.twig',
                            'redirectRoute': 'odiseo_sylius_blog_plugin_shop_article_show',
                            'redirectParameters': {'slug': comment.article.slug }
                        })) }}
                    </div>
                </div>
            </div>
        {% endif %}
    </div>
    {% if comment.enabledChildren|length > 0 %}
        <div class=\"comments\">
            {% for comment in comment.enabledChildren %}
                {% include '@OdiseoSyliusBlogPlugin/Shop/Article/_comment.html.twig' with {'comment': comment, 'withReply': false} %}
            {% endfor %}
        </div>
    {% endif %}
</div>
", "OdiseoSyliusBlogPlugin:Shop/Article:_comment.html.twig", "D:\\Formation\\Sylius\\vendor\\odiseoteam\\sylius-blog-plugin\\src\\Resources\\views\\Shop\\Article\\_comment.html.twig");
    }
}
