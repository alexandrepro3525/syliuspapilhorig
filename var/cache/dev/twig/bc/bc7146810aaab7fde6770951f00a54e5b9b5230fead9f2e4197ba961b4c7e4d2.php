<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OdiseoSyliusBlogPlugin:Shop/Article:_latest.html.twig */
class __TwigTemplate_f722c7735a3b3ac99cc221ba0f5faa2ff94fd029da8da915c1177dc60aaadcbe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:_latest.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:_latest.html.twig"));

        // line 1
        if ((twig_length_filter($this->env, (isset($context["articles"]) || array_key_exists("articles", $context) ? $context["articles"] : (function () { throw new RuntimeError('Variable "articles" does not exist.', 1, $this->source); })())) > 0)) {
            // line 2
            echo "    <div class=\"ui divided items\">
        ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) || array_key_exists("articles", $context) ? $context["articles"] : (function () { throw new RuntimeError('Variable "articles" does not exist.', 3, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
                // line 4
                echo "            <div class=\"item\">
                ";
                // line 5
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "images", [], "any", false, false, false, 5)) > 0)) {
                    // line 6
                    echo "                    <div class=\"ui tiny image\">
                        <img src=\"";
                    // line 7
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["article"], "images", [], "any", false, false, false, 7), 0, [], "array", false, false, false, 7), "path", [], "any", false, false, false, 7), (((isset($context["filter"]) || array_key_exists("filter", $context))) ? (_twig_default_filter((isset($context["filter"]) || array_key_exists("filter", $context) ? $context["filter"] : (function () { throw new RuntimeError('Variable "filter" does not exist.', 7, $this->source); })()), "sylius_small")) : ("sylius_small"))), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["article"], "images", [], "any", false, false, false, 7), 0, [], "array", false, false, false, 7), "path", [], "any", false, false, false, 7), "html", null, true);
                    echo "\">
                    </div>
                ";
                }
                // line 10
                echo "                <div class=\"content\">
                    <a class=\"header\" href=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_show", ["slug" => twig_get_attribute($this->env, $this->source, $context["article"], "slug", [], "any", false, false, false, 11)]), "html", null, true);
                echo "\">
                        ";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "title", [], "any", false, false, false, 12), "html", null, true);
                echo "
                    </a>
                    <div class=\"meta\">
                        <span>";
                // line 15
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "createdAt", [], "any", false, false, false, 15)), "html", null, true);
                echo "</span>
                    </div>
                </div>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "    </div>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OdiseoSyliusBlogPlugin:Shop/Article:_latest.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 20,  81 => 15,  75 => 12,  71 => 11,  68 => 10,  60 => 7,  57 => 6,  55 => 5,  52 => 4,  48 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if articles|length > 0 %}
    <div class=\"ui divided items\">
        {% for article in articles %}
            <div class=\"item\">
                {% if article.images|length > 0 %}
                    <div class=\"ui tiny image\">
                        <img src=\"{{ article.images[0].path|imagine_filter(filter|default('sylius_small')) }}\" alt=\"{{ article.images[0].path }}\">
                    </div>
                {% endif %}
                <div class=\"content\">
                    <a class=\"header\" href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_show', {'slug' : article.slug}) }}\">
                        {{ article.title }}
                    </a>
                    <div class=\"meta\">
                        <span>{{ article.createdAt|date }}</span>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
{% endif %}
", "OdiseoSyliusBlogPlugin:Shop/Article:_latest.html.twig", "D:\\Formation\\Sylius\\vendor\\odiseoteam\\sylius-blog-plugin\\src/Resources/views/Shop/Article/_latest.html.twig");
    }
}
