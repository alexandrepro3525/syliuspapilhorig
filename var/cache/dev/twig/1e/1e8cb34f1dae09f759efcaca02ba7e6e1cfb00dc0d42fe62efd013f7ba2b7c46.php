<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OdiseoSyliusBlogPlugin:Shop:_sidebar.html.twig */
class __TwigTemplate_b886eb12304a9dff2c31acd899db325b6d565818a283f35f3010349c0300f929 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop:_sidebar.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop:_sidebar.html.twig"));

        // line 1
        echo "<div class=\"ui secondary segment\">
    <h4 class=\"ui header\">
        ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.blog.about.title"), "html", null, true);
        echo "
    </h4>
    <p>
        ";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.blog.about.content");
        echo "
    </p>
</div>
<div class=\"ui segment\">
    <h4 class=\"ui header\">
        ";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.latest_articles"), "html", null, true);
        echo "
    </h4>
    ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("odiseo_sylius_blog_plugin_shop_partial_article_index_latest", ["count" => 4, "template" => "@OdiseoSyliusBlogPlugin/Shop/Article/_latest.html.twig"]));
        echo "
</div>
<div class=\"ui basic segment\">
    <h4 class=\"ui header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.article_categories"), "html", null, true);
        echo "</h4>
    ";
        // line 17
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("odiseo_sylius_blog_plugin_shop_partial_article_category_index", ["template" => "@OdiseoSyliusBlogPlugin/Shop/ArticleCategory/_verticalMenu.html.twig"]));
        echo "
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OdiseoSyliusBlogPlugin:Shop:_sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 17,  72 => 16,  66 => 13,  61 => 11,  53 => 6,  47 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"ui secondary segment\">
    <h4 class=\"ui header\">
        {{ 'odiseo_sylius_blog_plugin.ui.blog.about.title'|trans }}
    </h4>
    <p>
        {{ 'odiseo_sylius_blog_plugin.ui.blog.about.content'|trans|raw }}
    </p>
</div>
<div class=\"ui segment\">
    <h4 class=\"ui header\">
        {{ 'odiseo_sylius_blog_plugin.ui.latest_articles'|trans }}
    </h4>
    {{ render(url('odiseo_sylius_blog_plugin_shop_partial_article_index_latest', {'count': 4, 'template': '@OdiseoSyliusBlogPlugin/Shop/Article/_latest.html.twig'})) }}
</div>
<div class=\"ui basic segment\">
    <h4 class=\"ui header\">{{ 'odiseo_sylius_blog_plugin.ui.article_categories'|trans }}</h4>
    {{ render(url('odiseo_sylius_blog_plugin_shop_partial_article_category_index', {'template': '@OdiseoSyliusBlogPlugin/Shop/ArticleCategory/_verticalMenu.html.twig'})) }}
</div>
", "OdiseoSyliusBlogPlugin:Shop:_sidebar.html.twig", "D:\\Formation\\Sylius\\vendor\\odiseoteam\\sylius-blog-plugin\\src\\Resources\\views\\Shop\\_sidebar.html.twig");
    }
}
