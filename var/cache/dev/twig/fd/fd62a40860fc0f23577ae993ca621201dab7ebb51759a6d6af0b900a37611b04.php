<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Common/Order/Table:_totals.html.twig */
class __TwigTemplate_d783baf34abb1f444489e9d1f014119fd2f943b3e85da1e8c30e63120e68187d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Order/Table:_totals.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Order/Table:_totals.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Common/Order/Table:_totals.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["itemsSubtotal"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderItemsSubtotalExtension']->getSubtotal((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 3, $this->source); })()));
        // line 4
        $context["taxIncluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getIncludedTax((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 4, $this->source); })()));
        // line 5
        $context["taxExcluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getExcludedTax((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 5, $this->source); })()));
        // line 6
        echo "
";
        // line 7
        $context["orderPromotionAdjustment"] = twig_constant("Sylius\\Component\\Core\\Model\\AdjustmentInterface::ORDER_PROMOTION_ADJUSTMENT");
        // line 8
        $context["orderPromotions"] = call_user_func_array($this->env->getFunction('sylius_aggregate_adjustments')->getCallable(), [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 8, $this->source); })()), "adjustmentsRecursively", [0 => (isset($context["orderPromotionAdjustment"]) || array_key_exists("orderPromotionAdjustment", $context) ? $context["orderPromotionAdjustment"] : (function () { throw new RuntimeError('Variable "orderPromotionAdjustment" does not exist.', 8, $this->source); })())], "method", false, false, false, 8)]);
        // line 9
        echo "
<tr>
    <th colspan=\"4\" class=\"right aligned\" id=\"subtotal\">
        ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.items_total"), "html", null, true);
        echo ": ";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["itemsSubtotal"]) || array_key_exists("itemsSubtotal", $context) ? $context["itemsSubtotal"] : (function () { throw new RuntimeError('Variable "itemsSubtotal" does not exist.', 12, $this->source); })())], 12, $context, $this->getSourceContext());
        echo "
    </th>
</tr>
<tr";
        // line 15
        if (((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 15, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 15, $this->source); })()))) {
            echo "class=\"tax-disabled\"";
        }
        echo ">
    <td colspan=\"4\" class=\"right aligned\" id=\"tax-total\">
        <div style=\"display: flex; justify-content: flex-end; align-items: center\">
            <div>";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.taxes_total"), "html", null, true);
        echo ":&nbsp;</div>
            <div data-test=\"tax-total\">
            ";
        // line 20
        if (( !(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 20, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 20, $this->source); })()))) {
            // line 21
            echo "                <div id=\"sylius-cart-tax-none\">";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [0], 21, $context, $this->getSourceContext());
            echo "</div>
            ";
        }
        // line 23
        echo "            ";
        if ((isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 23, $this->source); })())) {
            // line 24
            echo "                <div id=\"sylius-cart-tax-excluded\">";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 24, $this->source); })())], 24, $context, $this->getSourceContext());
            echo "</div>
            ";
        }
        // line 26
        echo "            ";
        if ((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 26, $this->source); })())) {
            // line 27
            echo "                <div class=\"tax-disabled\">
                    <small>(";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.included_in_price"), "html", null, true);
            echo ")</small>
                    <span id=\"sylius-cart-tax-included\">";
            // line 29
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 29, $this->source); })())], 29, $context, $this->getSourceContext());
            echo "</span>
                </div>
            ";
        }
        // line 32
        echo "            </div>
        </div>
    </td>
</tr>
<tr>
    <td colspan=\"4\" id=\"promotion-total\" class=\"right aligned\">
        ";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.discount"), "html", null, true);
        echo ": ";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 38, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 38)], 38, $context, $this->getSourceContext());
        echo "
        ";
        // line 39
        if ((twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 39, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 39) != 0)) {
            // line 40
            echo "            <i id=\"order-promotions-details\" class=\"question circle icon popup-js\"
                data-html=\"";
            // line 41
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orderPromotions"]) || array_key_exists("orderPromotions", $context) ? $context["orderPromotions"] : (function () { throw new RuntimeError('Variable "orderPromotions" does not exist.', 41, $this->source); })()));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                echo "<div>";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo ": ";
                echo twig_call_macro($macros["money"], "macro_convertAndFormat", [$context["value"]], 41, $context, $this->getSourceContext());
                echo "</div>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
            </i>
        ";
        }
        // line 44
        echo "    </td>
</tr>
<tr>
    ";
        // line 47
        $this->loadTemplate("@SyliusShop/Common/Order/Table/_shipping.html.twig", "SyliusShopBundle:Common/Order/Table:_totals.html.twig", 47)->display(twig_array_merge($context, ["order" => (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 47, $this->source); })())]));
        // line 48
        echo "</tr>
<tr>
    <td colspan=\"4\" class=\"right aligned\" style=\"font-size: 1.5em;\" id=\"total\">
        ";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.total"), "html", null, true);
        echo ": ";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 51, $this->source); })()), "total", [], "any", false, false, false, 51)], 51, $context, $this->getSourceContext());
        echo "
    </td>
</tr>
";
        // line 54
        if ( !(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 54, $this->source); })()), "currencyCode", [], "any", false, false, false, 54) === twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 54, $this->source); })()), "currencyCode", [], "any", false, false, false, 54))) {
            // line 55
            echo "    <tr>
        <td colspan=\"4\" class=\"right aligned\" id=\"base-total\">
            ";
            // line 57
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.total_in_base_currency"), "html", null, true);
            echo ": ";
            echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 57, $this->source); })()), "total", [], "any", false, false, false, 57), twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 57, $this->source); })()), "currencyCode", [], "any", false, false, false, 57)], 57, $context, $this->getSourceContext());
            echo "
        </td>
    </tr>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Common/Order/Table:_totals.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 57,  178 => 55,  176 => 54,  168 => 51,  163 => 48,  161 => 47,  156 => 44,  139 => 41,  136 => 40,  134 => 39,  128 => 38,  120 => 32,  114 => 29,  110 => 28,  107 => 27,  104 => 26,  98 => 24,  95 => 23,  89 => 21,  87 => 20,  82 => 18,  74 => 15,  66 => 12,  61 => 9,  59 => 8,  57 => 7,  54 => 6,  52 => 5,  50 => 4,  48 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}

{% set itemsSubtotal = sylius_order_items_subtotal(order) %}
{% set taxIncluded = sylius_order_tax_included(order) %}
{% set taxExcluded = sylius_order_tax_excluded(order) %}

{% set orderPromotionAdjustment = constant('Sylius\\\\Component\\\\Core\\\\Model\\\\AdjustmentInterface::ORDER_PROMOTION_ADJUSTMENT') %}
{% set orderPromotions = sylius_aggregate_adjustments(order.adjustmentsRecursively(orderPromotionAdjustment)) %}

<tr>
    <th colspan=\"4\" class=\"right aligned\" id=\"subtotal\">
        {{ 'sylius.ui.items_total'|trans }}: {{ money.convertAndFormat(itemsSubtotal) }}
    </th>
</tr>
<tr{% if taxIncluded and not taxExcluded %}class=\"tax-disabled\"{% endif %}>
    <td colspan=\"4\" class=\"right aligned\" id=\"tax-total\">
        <div style=\"display: flex; justify-content: flex-end; align-items: center\">
            <div>{{ 'sylius.ui.taxes_total'|trans }}:&nbsp;</div>
            <div data-test=\"tax-total\">
            {% if not taxIncluded and not taxExcluded %}
                <div id=\"sylius-cart-tax-none\">{{ money.convertAndFormat(0) }}</div>
            {% endif %}
            {% if taxExcluded %}
                <div id=\"sylius-cart-tax-excluded\">{{ money.convertAndFormat(taxExcluded) }}</div>
            {% endif %}
            {% if taxIncluded %}
                <div class=\"tax-disabled\">
                    <small>({{ 'sylius.ui.included_in_price'|trans }})</small>
                    <span id=\"sylius-cart-tax-included\">{{ money.convertAndFormat(taxIncluded) }}</span>
                </div>
            {% endif %}
            </div>
        </div>
    </td>
</tr>
<tr>
    <td colspan=\"4\" id=\"promotion-total\" class=\"right aligned\">
        {{ 'sylius.ui.discount'|trans }}: {{ money.convertAndFormat(order.orderPromotionTotal)  }}
        {% if order.orderPromotionTotal != 0 %}
            <i id=\"order-promotions-details\" class=\"question circle icon popup-js\"
                data-html=\"{% for key, value in orderPromotions %}<div>{{ key }}: {{ money.convertAndFormat(value) }}</div>{% endfor %}\">
            </i>
        {% endif %}
    </td>
</tr>
<tr>
    {% include '@SyliusShop/Common/Order/Table/_shipping.html.twig' with {'order': order} %}
</tr>
<tr>
    <td colspan=\"4\" class=\"right aligned\" style=\"font-size: 1.5em;\" id=\"total\">
        {{ 'sylius.ui.total'|trans }}: {{ money.convertAndFormat(order.total) }}
    </td>
</tr>
{% if order.currencyCode is not same as(sylius.currencyCode) %}
    <tr>
        <td colspan=\"4\" class=\"right aligned\" id=\"base-total\">
            {{ 'sylius.ui.total_in_base_currency'|trans }}: {{ money.format(order.total, order.currencyCode) }}
        </td>
    </tr>
{% endif %}
", "SyliusShopBundle:Common/Order/Table:_totals.html.twig", "D:\\Formation\\Sylius\\vendor\\sylius\\sylius\\src\\Sylius\\Bundle\\ShopBundle/Resources/views/Common/Order/Table/_totals.html.twig");
    }
}
