<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig */
class __TwigTemplate_9c1e2db89f4ef16480ffd86d554863175047fefd0696cfb3d6d55535c3e49048 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'metatags' => [$this, 'block_metatags'],
            'content' => [$this, 'block_content'],
            'blog_breadcrumb' => [$this, 'block_blog_breadcrumb'],
            'blog_article' => [$this, 'block_blog_article'],
            'blog_disqus' => [$this, 'block_blog_disqus'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_metatags($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metatags"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metatags"));

        // line 4
        echo "    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 4, $this->source); })()), "metaKeywords", [], "any", false, false, false, 4))) {
            // line 5
            echo "        <meta name=\"keywords\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 5, $this->source); })()), "metaKeywords", [], "any", false, false, false, 5), "html", null, true);
            echo "\"/>
    ";
        }
        // line 7
        echo "    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 7, $this->source); })()), "metaDescription", [], "any", false, false, false, 7))) {
            // line 8
            echo "        <meta name=\"description\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 8, $this->source); })()), "metaDescription", [], "any", false, false, false, 8), "html", null, true);
            echo "\"/>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 13
        echo "    ";
        $this->displayBlock('blog_breadcrumb', $context, $blocks);
        // line 22
        echo "
    ";
        // line 23
        $this->displayBlock('blog_article', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_blog_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_breadcrumb"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_breadcrumb"));

        // line 14
        echo "        <div class=\"ui breadcrumb\">
            <a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_homepage");
        echo "\" class=\"section\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.home"), "html", null, true);
        echo "</a>
            <div class=\"divider\"> /</div>
            <a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_index");
        echo "\" class=\"section\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.blog.header"), "html", null, true);
        echo "</a>
            <div class=\"divider\"> /</div>
            <div class=\"active section\">";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 19, $this->source); })()), "title", [], "any", false, false, false, 19), "html", null, true);
        echo "</div>
        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 23
    public function block_blog_article($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_article"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_article"));

        // line 24
        echo "        <div class=\"ui grid\">
            <div class=\"sixteen wide mobile twelve wide computer column\">
                <div class=\"ui segment\">
                    <h1 class=\"ui dividing header\">";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 27, $this->source); })()), "title", [], "any", false, false, false, 27), "html", null, true);
        echo "</h1>
                    <div class=\"ui items\">
                        <div class=\"item\">
                            <div class=\"content\">
                                <div class=\"meta ui right\">
                                    <span>";
        // line 32
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 32, $this->source); })()), "createdAt", [], "any", false, false, false, 32)), "html", null, true);
        echo "</span>
                                    ";
        // line 33
        if (twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 33, $this->source); })()), "author", [], "any", false, false, false, 33)) {
            // line 34
            echo "                                        <span>by <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_index_by_author", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 34, $this->source); })()), "author", [], "any", false, false, false, 34), "username", [], "any", false, false, false, 34)]), "html", null, true);
            echo "\"><strong>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 34, $this->source); })()), "author", [], "any", false, false, false, 34), "username", [], "any", false, false, false, 34), "html", null, true);
            echo "</strong></a> </span>
                                    ";
        }
        // line 36
        echo "                                </div>
                                <div class=\"description\">
                                    ";
        // line 38
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 38, $this->source); })()), "images", [], "any", false, false, false, 38)) > 0)) {
            // line 39
            echo "                                        <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 39, $this->source); })()), "images", [], "any", false, false, false, 39), 0, [], "array", false, false, false, 39), "path", [], "any", false, false, false, 39), (((isset($context["filter"]) || array_key_exists("filter", $context))) ? (_twig_default_filter((isset($context["filter"]) || array_key_exists("filter", $context) ? $context["filter"] : (function () { throw new RuntimeError('Variable "filter" does not exist.', 39, $this->source); })()), "sylius_large")) : ("sylius_large"))), "html", null, true);
            echo "\" class=\"ui image centered\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 39, $this->source); })()), "images", [], "any", false, false, false, 39), 0, [], "array", false, false, false, 39), "path", [], "any", false, false, false, 39), "html", null, true);
            echo "\">
                                        <div class=\"ui hidden divider\"></div>
                                    ";
        }
        // line 42
        echo "                                    <p>";
        echo twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 42, $this->source); })()), "content", [], "any", false, false, false, 42);
        echo "</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        // line 47
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 47, $this->source); })()), "categories", [], "any", false, false, false, 47)) > 0)) {
            // line 48
            echo "                        <div class=\"extra\">
                            <strong>";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.article_categories"), "html", null, true);
            echo ": </strong>
                            ";
            // line 50
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 50, $this->source); })()), "categories", [], "any", false, false, false, 50));
            foreach ($context['_seq'] as $context["_key"] => $context["articleCategory"]) {
                // line 51
                echo "                                <div class=\"ui label\"><a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_sylius_blog_plugin_shop_article_index_by_category", ["slug" => twig_get_attribute($this->env, $this->source, $context["articleCategory"], "slug", [], "any", false, false, false, 51)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articleCategory"], "title", [], "any", false, false, false, 51), "html", null, true);
                echo "</a></div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['articleCategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                        </div>
                    ";
        }
        // line 55
        echo "
                    ";
        // line 56
        if ($this->extensions['Odiseo\SyliusBlogPlugin\Twig\BlogExtension']->getDisqusShortname()) {
            // line 57
            echo "                        ";
            $this->displayBlock('blog_disqus', $context, $blocks);
            // line 60
            echo "                    ";
        } else {
            // line 61
            echo "                        <div class=\"ui comments\" id=\"blog-comments\">
                            <h3 class=\"ui dividing header\">";
            // line 62
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_blog.ui.article_comments"), "html", null, true);
            echo "</h3>
                            ";
            // line 63
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 63, $this->source); })()), "enabledComments", [], "any", false, false, false, 63));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                // line 64
                echo "                                ";
                $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/Article/_comment.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig", 64)->display(twig_array_merge($context, ["comment" => $context["comment"], "withReply" => true]));
                // line 65
                echo "                            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "
                            <h4>";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.leave_comment");
            echo "</h4>

                            ";
            // line 69
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("odiseo_blog_partial_comment_create", ["articleId" => twig_get_attribute($this->env, $this->source,             // line 70
(isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 70, $this->source); })()), "id", [], "any", false, false, false, 70), "template" => "@OdiseoSyliusBlogPlugin/Shop/Article/_comment_create.html.twig", "redirectRoute" => "odiseo_sylius_blog_plugin_shop_article_show", "redirectParameters" => ["slug" => twig_get_attribute($this->env, $this->source,             // line 73
(isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 73, $this->source); })()), "slug", [], "any", false, false, false, 73)]]));
            // line 74
            echo "
                        </div>
                    ";
        }
        // line 77
        echo "                </div>
            </div>
            <div class=\"sixteen wide mobile four wide computer column\">
                ";
        // line 80
        $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/_sidebar.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig", 80)->display($context);
        // line 81
        echo "            </div>
        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 57
    public function block_blog_disqus($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_disqus"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "blog_disqus"));

        // line 58
        echo "                            ";
        $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/Article/_disqus.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig", 58)->display($context);
        // line 59
        echo "                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  354 => 59,  351 => 58,  341 => 57,  329 => 81,  327 => 80,  322 => 77,  317 => 74,  315 => 73,  314 => 70,  313 => 69,  308 => 67,  305 => 66,  291 => 65,  288 => 64,  271 => 63,  267 => 62,  264 => 61,  261 => 60,  258 => 57,  256 => 56,  253 => 55,  249 => 53,  238 => 51,  234 => 50,  230 => 49,  227 => 48,  225 => 47,  216 => 42,  207 => 39,  205 => 38,  201 => 36,  193 => 34,  191 => 33,  187 => 32,  179 => 27,  174 => 24,  164 => 23,  151 => 19,  144 => 17,  137 => 15,  134 => 14,  124 => 13,  114 => 23,  111 => 22,  108 => 13,  98 => 12,  84 => 8,  81 => 7,  75 => 5,  72 => 4,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% block metatags %}
    {% if article.metaKeywords is not empty %}
        <meta name=\"keywords\" content=\"{{ article.metaKeywords }}\"/>
    {% endif %}
    {% if article.metaDescription is not empty %}
        <meta name=\"description\" content=\"{{ article.metaDescription }}\"/>
    {% endif %}
{% endblock %}

{% block content %}
    {% block blog_breadcrumb %}
        <div class=\"ui breadcrumb\">
            <a href=\"{{ path('sylius_shop_homepage') }}\" class=\"section\">{{ 'sylius.ui.home'|trans }}</a>
            <div class=\"divider\"> /</div>
            <a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_index') }}\" class=\"section\">{{ 'odiseo_sylius_blog_plugin.ui.blog.header'|trans }}</a>
            <div class=\"divider\"> /</div>
            <div class=\"active section\">{{ article.title }}</div>
        </div>
    {% endblock %}

    {% block blog_article %}
        <div class=\"ui grid\">
            <div class=\"sixteen wide mobile twelve wide computer column\">
                <div class=\"ui segment\">
                    <h1 class=\"ui dividing header\">{{ article.title }}</h1>
                    <div class=\"ui items\">
                        <div class=\"item\">
                            <div class=\"content\">
                                <div class=\"meta ui right\">
                                    <span>{{ article.createdAt|date }}</span>
                                    {% if article.author %}
                                        <span>by <a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_index_by_author', {'username': article.author.username}) }}\"><strong>{{ article.author.username }}</strong></a> </span>
                                    {% endif %}
                                </div>
                                <div class=\"description\">
                                    {% if article.images|length > 0 %}
                                        <img src=\"{{ article.images[0].path|imagine_filter(filter|default('sylius_large')) }}\" class=\"ui image centered\" alt=\"{{ article.images[0].path }}\">
                                        <div class=\"ui hidden divider\"></div>
                                    {% endif %}
                                    <p>{{ article.content|raw }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% if article.categories|length > 0 %}
                        <div class=\"extra\">
                            <strong>{{ 'odiseo_sylius_blog_plugin.ui.article_categories'|trans }}: </strong>
                            {% for articleCategory in article.categories %}
                                <div class=\"ui label\"><a href=\"{{ path('odiseo_sylius_blog_plugin_shop_article_index_by_category', {'slug': articleCategory.slug}) }}\">{{ articleCategory.title }}</a></div>
                            {% endfor %}
                        </div>
                    {% endif %}

                    {% if disqus_shortname() %}
                        {% block blog_disqus %}
                            {% include '@OdiseoSyliusBlogPlugin/Shop/Article/_disqus.html.twig' %}
                        {% endblock %}
                    {% else %}
                        <div class=\"ui comments\" id=\"blog-comments\">
                            <h3 class=\"ui dividing header\">{{ 'odiseo_blog.ui.article_comments'|trans }}</h3>
                            {% for comment in article.enabledComments %}
                                {% include '@OdiseoSyliusBlogPlugin/Shop/Article/_comment.html.twig' with {'comment': comment, 'withReply': true} %}
                            {% endfor %}

                            <h4>{{ 'odiseo_sylius_blog_plugin.ui.leave_comment'|trans|raw }}</h4>

                            {{ render(url('odiseo_blog_partial_comment_create', {
                                'articleId': article.id,
                                'template': '@OdiseoSyliusBlogPlugin/Shop/Article/_comment_create.html.twig',
                                'redirectRoute': 'odiseo_sylius_blog_plugin_shop_article_show',
                                'redirectParameters': {'slug': article.slug }
                            })) }}
                        </div>
                    {% endif %}
                </div>
            </div>
            <div class=\"sixteen wide mobile four wide computer column\">
                {% include '@OdiseoSyliusBlogPlugin/Shop/_sidebar.html.twig' %}
            </div>
        </div>
    {% endblock %}
{% endblock %}
", "OdiseoSyliusBlogPlugin:Shop/Article:show.html.twig", "D:\\Formation\\Sylius\\vendor\\odiseoteam\\sylius-blog-plugin\\src\\Resources\\views\\Shop\\Article\\show.html.twig");
    }
}
