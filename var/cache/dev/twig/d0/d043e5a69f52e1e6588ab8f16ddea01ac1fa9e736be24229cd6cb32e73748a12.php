<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OdiseoSyliusBlogPlugin:Shop/Article:_comment_create.html.twig */
class __TwigTemplate_0fccc9064acc7a1e4f3d83c0c7baec02786bf1ec2bc90de05e30fa79e95d150e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:_comment_create.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OdiseoSyliusBlogPlugin:Shop/Article:_comment_create.html.twig"));

        // line 1
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), [0 => "@SyliusShop/Form/theme.html.twig"], true);
        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("odiseo_blog_partial_comment_create", ["articleId" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 4
(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "vars", [], "any", false, false, false, 4), "data", [], "any", false, false, false, 4), "article", [], "any", false, false, false, 4)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "vars", [], "any", false, false, false, 4), "data", [], "any", false, false, false, 4), "article", [], "any", false, false, false, 4), "id", [], "any", false, false, false, 4)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "vars", [], "any", false, false, false, 4), "data", [], "any", false, false, false, 4), "parent", [], "any", false, false, false, 4), "article", [], "any", false, false, false, 4), "id", [], "any", false, false, false, 4))), "commentId" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 5
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "request", [], "any", false, false, false, 5), "get", [0 => "commentId"], "method", false, false, false, 5), "template" => "@OdiseoSyliusBlogPlugin/Shop/Article/_comment_create.html.twig", "redirectRoute" => "odiseo_sylius_blog_plugin_shop_article_show", "redirectParameters" => ["slug" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 8
(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "vars", [], "any", false, false, false, 8), "data", [], "any", false, false, false, 8), "article", [], "any", false, false, false, 8)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "vars", [], "any", false, false, false, 8), "data", [], "any", false, false, false, 8), "article", [], "any", false, false, false, 8), "slug", [], "any", false, false, false, 8)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "vars", [], "any", false, false, false, 8), "data", [], "any", false, false, false, 8), "parent", [], "any", false, false, false, 8), "article", [], "any", false, false, false, 8), "slug", [], "any", false, false, false, 8)))]]), "method" => "POST", "attr" => ["class" => "ui reply form"]]);
        // line 9
        echo "
    ";
        // line 10
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), "vars", [], "any", false, false, false, 10), "data", [], "any", false, false, false, 10), "author", [], "any", false, false, false, 10)) {
            // line 11
            echo "        ";
            $this->loadTemplate("@OdiseoSyliusBlogPlugin/Shop/Article/_comment_author.html.twig", "OdiseoSyliusBlogPlugin:Shop/Article:_comment_create.html.twig", 11)->display(twig_array_merge($context, ["author" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "vars", [], "any", false, false, false, 11), "data", [], "any", false, false, false, 11), "author", [], "any", false, false, false, 11)]));
            // line 12
            echo "    ";
        }
        // line 13
        echo "
    ";
        // line 14
        if ((twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", true, true, false, 14) || twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", true, true, false, 14))) {
            // line 15
            echo "        <div class=\"two fields\">
            ";
            // line 16
            if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", true, true, false, 16)) {
                // line 17
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 17, $this->source); })()), "name", [], "any", false, false, false, 17), 'row');
                echo "
            ";
            }
            // line 19
            echo "            ";
            if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", true, true, false, 19)) {
                // line 20
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 20, $this->source); })()), "email", [], "any", false, false, false, 20), 'row');
                echo "
            ";
            }
            // line 22
            echo "        </div>
    ";
        }
        // line 24
        echo "
    ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "comment", [], "any", false, false, false, 25), 'row');
        echo "

    ";
        // line 27
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "recaptcha", [], "any", true, true, false, 27)) {
            // line 28
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 28, $this->source); })()), "recaptcha", [], "any", false, false, false, 28), 'row');
            echo "
    ";
        }
        // line 30
        echo "
    <button type=\"submit\" class=\"ui blue labeled submit icon button\">
        <i class=\"icon edit\"></i> ";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("odiseo_sylius_blog_plugin.ui.add_comment"), "html", null, true);
        echo "
    </button>
";
        // line 34
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 34, $this->source); })()), 'form_end', ["render_rest" => true]);
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OdiseoSyliusBlogPlugin:Shop/Article:_comment_create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 34,  113 => 32,  109 => 30,  103 => 28,  101 => 27,  96 => 25,  93 => 24,  89 => 22,  83 => 20,  80 => 19,  74 => 17,  72 => 16,  69 => 15,  67 => 14,  64 => 13,  61 => 12,  58 => 11,  56 => 10,  53 => 9,  51 => 8,  50 => 5,  49 => 4,  48 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% form_theme form '@SyliusShop/Form/theme.html.twig' %}

{{ form_start(form, {'action': path('odiseo_blog_partial_comment_create', {
    'articleId': form.vars.data.article ? form.vars.data.article.id : form.vars.data.parent.article.id,
    'commentId': app.request.get('commentId'),
    'template': '@OdiseoSyliusBlogPlugin/Shop/Article/_comment_create.html.twig',
    'redirectRoute': 'odiseo_sylius_blog_plugin_shop_article_show',
    'redirectParameters': {'slug': form.vars.data.article?form.vars.data.article.slug:form.vars.data.parent.article.slug }
}), 'method': 'POST', 'attr': {'class': 'ui reply form'}}) }}
    {% if form.vars.data.author %}
        {% include '@OdiseoSyliusBlogPlugin/Shop/Article/_comment_author.html.twig' with {'author': form.vars.data.author} %}
    {% endif %}

    {% if form.name is defined or form.email is defined %}
        <div class=\"two fields\">
            {% if form.name is defined %}
                {{ form_row(form.name) }}
            {% endif %}
            {% if form.email is defined %}
                {{ form_row(form.email) }}
            {% endif %}
        </div>
    {% endif %}

    {{ form_row(form.comment) }}

    {% if form.recaptcha is defined %}
        {{ form_row(form.recaptcha) }}
    {% endif %}

    <button type=\"submit\" class=\"ui blue labeled submit icon button\">
        <i class=\"icon edit\"></i> {{ 'odiseo_sylius_blog_plugin.ui.add_comment'|trans }}
    </button>
{{ form_end(form, {'render_rest': true}) }}
", "OdiseoSyliusBlogPlugin:Shop/Article:_comment_create.html.twig", "D:\\Formation\\Sylius\\vendor\\odiseoteam\\sylius-blog-plugin\\src/Resources/views/Shop/Article/_comment_create.html.twig");
    }
}
