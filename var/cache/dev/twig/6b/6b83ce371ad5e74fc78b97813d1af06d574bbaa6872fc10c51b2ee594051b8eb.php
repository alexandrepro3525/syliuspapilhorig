<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photobook/photobook.html.twig */
class __TwigTemplate_3a300fac552798249d64c16630bfe53c6ef8cdc068282b6c8f96ee8bbabfc5eb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photobook/photobook.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photobook/photobook.html.twig"));

        // line 1
        $this->loadTemplate("@SyliusShop/_header.html.twig", "photobook/photobook.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "<div class=\"row\">
    <div class=\"col-sm-6 categorie\">
        <h3 id=\"h3\">categorie 1</h3>    
    </div>
        <div class=\"col-sm-6\">
            <img id=\"pictures\" src=\"/assets/img/categorie1/";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["img1"]) || array_key_exists("img1", $context) ? $context["img1"] : (function () { throw new RuntimeError('Variable "img1" does not exist.', 13, $this->source); })()), "html", null, true);
        echo "\" alt=\"dfd\"><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categorie1");
        echo "\" ><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
            style=\"
                    z-index: 2;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%); 
                \">
            </input></a>
        </div>

    <div class=\"col-sm-6\">
        <img id=\"pictures\" src=\"/assets/img/categorie2/";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["img2"]) || array_key_exists("img2", $context) ? $context["img2"] : (function () { throw new RuntimeError('Variable "img2" does not exist.', 25, $this->source); })()), "html", null, true);
        echo "\" alt=\"dfd\"><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categorie2");
        echo "\"><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
        style=\"
                z-index: 2;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%); 
            \">
        </input></a>
    </div>
        <div class=\"col-sm-6 categorie2\">
            <h3 id=\"h3\">categorie 2</h3>    
        </div>

    <div class=\"col-sm-6 categorie\">
        <h3 id=\"h3\">categorie 3</h3>    
    </div>
        <div class=\"col-sm-6\">
            <img id=\"pictures\" src=\"/assets/img/categorie3/";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["img3"]) || array_key_exists("img3", $context) ? $context["img3"] : (function () { throw new RuntimeError('Variable "img3" does not exist.', 43, $this->source); })()), "html", null, true);
        echo "\" alt=\"dfd\"><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categorie3");
        echo "\" target=\"_blank\"><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
            style=\"
                    z-index: 2;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%); 
                \">
            </input></a>
        </div>

        ";
        // line 68
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "photobook/photobook.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 68,  142 => 43,  119 => 25,  102 => 13,  95 => 8,  85 => 7,  72 => 5,  62 => 4,  52 => 7,  50 => 4,  47 => 2,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include \"@SyliusShop/_header.html.twig\" %}

{# {% block title %}Photobook{% endblock %} #}
{% block stylesheets %}
    <link rel=\"stylesheet\" href=\"{{asset('css/main.css')}}\">
{% endblock %}
{% block body %}
<div class=\"row\">
    <div class=\"col-sm-6 categorie\">
        <h3 id=\"h3\">categorie 1</h3>    
    </div>
        <div class=\"col-sm-6\">
            <img id=\"pictures\" src=\"/assets/img/categorie1/{{img1}}\" alt=\"dfd\"><a href=\"{{ path('categorie1') }}\" ><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
            style=\"
                    z-index: 2;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%); 
                \">
            </input></a>
        </div>

    <div class=\"col-sm-6\">
        <img id=\"pictures\" src=\"/assets/img/categorie2/{{img2}}\" alt=\"dfd\"><a href=\"{{ path('categorie2') }}\"><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
        style=\"
                z-index: 2;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%); 
            \">
        </input></a>
    </div>
        <div class=\"col-sm-6 categorie2\">
            <h3 id=\"h3\">categorie 2</h3>    
        </div>

    <div class=\"col-sm-6 categorie\">
        <h3 id=\"h3\">categorie 3</h3>    
    </div>
        <div class=\"col-sm-6\">
            <img id=\"pictures\" src=\"/assets/img/categorie3/{{img3}}\" alt=\"dfd\"><a href=\"{{ path('categorie3') }}\" target=\"_blank\"><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
            style=\"
                    z-index: 2;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%); 
                \">
            </input></a>
        </div>

        {# <div class=\"col-sm-6\">
        <img id=\"pictures\" src=\"/assets/img/categorie1/{{img}}\" alt=\"dfd\"><a href=\"{{ path('categorie4') }}\"><input type=\"button\" class=\"btn btn-primary\" value=\"Voir plus\"
        style=\"
                z-index: 2;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%); 
            \">
        </input></a>
    </div>
        <div class=\"col-sm-6 categorie2\">
            <h3 id=\"h3\">categorie 4</h3>    
        </div> #}
</div>
{% endblock %}", "photobook/photobook.html.twig", "D:\\Formation\\Sylius\\templates\\photobook\\photobook.html.twig");
    }
}
