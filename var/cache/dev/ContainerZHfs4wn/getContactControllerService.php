<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\ContactController' shared autowired service.

include_once \dirname(__DIR__, 4).'\\vendor\\symfony\\framework-bundle\\Controller\\ControllerTrait.php';
include_once \dirname(__DIR__, 4).'\\vendor\\symfony\\framework-bundle\\Controller\\AbstractController.php';
include_once \dirname(__DIR__, 4).'\\src\\Controller\\ContactController.php';

$this->services['App\\Controller\\ContactController'] = $instance = new \App\Controller\ContactController();

$instance->setContainer(($this->privates['.service_locator.WkgBPi_'] ?? $this->load('get_ServiceLocator_WkgBPiService.php'))->withContext('App\\Controller\\ContactController', $this));

return $instance;
