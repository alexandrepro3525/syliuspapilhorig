<?php
include_once \dirname(__DIR__, 4).'\\vendor\\theofidry\\alice-data-fixtures\\src\\LoaderInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\theofidry\\alice-data-fixtures\\src\\Persistence\\PersisterAwareInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\nelmio\\alice\\src\\IsAServiceTrait.php';
include_once \dirname(__DIR__, 4).'\\vendor\\theofidry\\alice-data-fixtures\\src\\Loader\\PersisterLoader.php';

class PersisterLoader_c8a8e24 extends \Fidry\AliceDataFixtures\Loader\PersisterLoader implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder78867 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera0131 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesd6273 = [
        
    ];

    public function withPersister(\Fidry\AliceDataFixtures\Persistence\PersisterInterface $persister) : \Fidry\AliceDataFixtures\Loader\PersisterLoader
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'withPersister', array('persister' => $persister), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->withPersister($persister);
    }

    public function load(array $fixturesFiles, array $parameters = [], array $objects = [], ?\Fidry\AliceDataFixtures\Persistence\PurgeMode $purgeMode = null) : array
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'load', array('fixturesFiles' => $fixturesFiles, 'parameters' => $parameters, 'objects' => $objects, 'purgeMode' => $purgeMode), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->load($fixturesFiles, $parameters, $objects, $purgeMode);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Fidry\AliceDataFixtures\Loader\PersisterLoader $instance) {
            unset($instance->loader, $instance->persister, $instance->logger, $instance->processors);
        }, $instance, 'Fidry\\AliceDataFixtures\\Loader\\PersisterLoader')->__invoke($instance);

        $instance->initializera0131 = $initializer;

        return $instance;
    }

    public function __construct(\Fidry\AliceDataFixtures\LoaderInterface $decoratedLoader, \Fidry\AliceDataFixtures\Persistence\PersisterInterface $persister, ?\Psr\Log\LoggerInterface $logger = null, array $processors = [])
    {
        static $reflection;

        if (! $this->valueHolder78867) {
            $reflection = $reflection ?? new \ReflectionClass('Fidry\\AliceDataFixtures\\Loader\\PersisterLoader');
            $this->valueHolder78867 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Fidry\AliceDataFixtures\Loader\PersisterLoader $instance) {
            unset($instance->loader, $instance->persister, $instance->logger, $instance->processors);
        }, $this, 'Fidry\\AliceDataFixtures\\Loader\\PersisterLoader')->__invoke($this);

        }

        $this->valueHolder78867->__construct($decoratedLoader, $persister, $logger, $processors);
    }

    public function & __get($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__get', ['name' => $name], $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        if (isset(self::$publicPropertiesd6273[$name])) {
            return $this->valueHolder78867->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__isset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__unset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__clone', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $this->valueHolder78867 = clone $this->valueHolder78867;
    }

    public function __sleep()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__sleep', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return array('valueHolder78867');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Fidry\AliceDataFixtures\Loader\PersisterLoader $instance) {
            unset($instance->loader, $instance->persister, $instance->logger, $instance->processors);
        }, $this, 'Fidry\\AliceDataFixtures\\Loader\\PersisterLoader')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializera0131 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializera0131;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'initializeProxy', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder78867;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder78867;
    }


}
