<?php
include_once \dirname(__DIR__, 4).'\\vendor\\doctrine\\persistence\\lib\\Doctrine\\Common\\Persistence\\ObjectManager.php';
include_once \dirname(__DIR__, 4).'\\vendor\\doctrine\\orm\\lib\\Doctrine\\ORM\\EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\doctrine\\orm\\lib\\Doctrine\\ORM\\EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder78867 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera0131 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesd6273 = [
        
    ];

    public function getConnection()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getConnection', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getMetadataFactory', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getExpressionBuilder', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'beginTransaction', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->beginTransaction();
    }

    public function getCache()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getCache', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getCache();
    }

    public function transactional($func)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'transactional', array('func' => $func), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->transactional($func);
    }

    public function commit()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'commit', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->commit();
    }

    public function rollback()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'rollback', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getClassMetadata', array('className' => $className), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'createQuery', array('dql' => $dql), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'createNamedQuery', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'createQueryBuilder', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'flush', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->flush($entity);
    }

    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->find($entityName, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'clear', array('entityName' => $entityName), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->clear($entityName);
    }

    public function close()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'close', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->close();
    }

    public function persist($entity)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'persist', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'remove', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'refresh', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'detach', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'merge', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getRepository', array('entityName' => $entityName), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'contains', array('entity' => $entity), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getEventManager', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getConfiguration', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'isOpen', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getUnitOfWork', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getProxyFactory', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'initializeObject', array('obj' => $obj), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getFilters', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'isFiltersStateClean', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'hasFilters', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializera0131 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder78867) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder78867 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder78867->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__get', ['name' => $name], $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        if (isset(self::$publicPropertiesd6273[$name])) {
            return $this->valueHolder78867->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__isset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__unset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__clone', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $this->valueHolder78867 = clone $this->valueHolder78867;
    }

    public function __sleep()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__sleep', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return array('valueHolder78867');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializera0131 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializera0131;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'initializeProxy', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder78867;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder78867;
    }


}
