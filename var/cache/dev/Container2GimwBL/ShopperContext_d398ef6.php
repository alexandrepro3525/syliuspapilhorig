<?php
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\sylius\\src\\Sylius\\Component\\Core\\Context\\ShopperContextInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\sylius\\src\\Sylius\\Component\\Core\\Context\\ShopperContext.php';

class ShopperContext_d398ef6 extends \Sylius\Component\Core\Context\ShopperContext implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder142bd = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer90675 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties2e9c3 = [
        
    ];

    public function getChannel() : \Sylius\Component\Channel\Model\ChannelInterface
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getChannel', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getChannel();
    }

    public function getCurrencyCode() : string
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getCurrencyCode', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getCurrencyCode();
    }

    public function getLocaleCode() : string
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getLocaleCode', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getLocaleCode();
    }

    public function getCustomer() : ?\Sylius\Component\Customer\Model\CustomerInterface
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getCustomer', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getCustomer();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Sylius\Component\Core\Context\ShopperContext $instance) {
            unset($instance->channelContext, $instance->currencyContext, $instance->localeContext, $instance->customerContext);
        }, $instance, 'Sylius\\Component\\Core\\Context\\ShopperContext')->__invoke($instance);

        $instance->initializer90675 = $initializer;

        return $instance;
    }

    public function __construct(\Sylius\Component\Channel\Context\ChannelContextInterface $channelContext, \Sylius\Component\Currency\Context\CurrencyContextInterface $currencyContext, \Sylius\Component\Locale\Context\LocaleContextInterface $localeContext, \Sylius\Component\Customer\Context\CustomerContextInterface $customerContext)
    {
        static $reflection;

        if (! $this->valueHolder142bd) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Component\\Core\\Context\\ShopperContext');
            $this->valueHolder142bd = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Sylius\Component\Core\Context\ShopperContext $instance) {
            unset($instance->channelContext, $instance->currencyContext, $instance->localeContext, $instance->customerContext);
        }, $this, 'Sylius\\Component\\Core\\Context\\ShopperContext')->__invoke($this);

        }

        $this->valueHolder142bd->__construct($channelContext, $currencyContext, $localeContext, $customerContext);
    }

    public function & __get($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__get', ['name' => $name], $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        if (isset(self::$publicProperties2e9c3[$name])) {
            return $this->valueHolder142bd->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__isset', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__unset', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__clone', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $this->valueHolder142bd = clone $this->valueHolder142bd;
    }

    public function __sleep()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__sleep', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return array('valueHolder142bd');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Sylius\Component\Core\Context\ShopperContext $instance) {
            unset($instance->channelContext, $instance->currencyContext, $instance->localeContext, $instance->customerContext);
        }, $this, 'Sylius\\Component\\Core\\Context\\ShopperContext')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer90675 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer90675;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'initializeProxy', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder142bd;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder142bd;
    }


}
