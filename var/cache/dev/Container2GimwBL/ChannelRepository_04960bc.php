<?php
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\sylius\\src\\Sylius\\Component\\Channel\\Repository\\ChannelRepositoryInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\sylius\\src\\Sylius\\Bundle\\ChannelBundle\\Doctrine\\ORM\\ChannelRepository.php';

class ChannelRepository_04960bc extends \Sylius\Bundle\ChannelBundle\Doctrine\ORM\ChannelRepository implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder142bd = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer90675 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties2e9c3 = [
        
    ];

    public function findOneByHostname(string $hostname) : ?\Sylius\Component\Channel\Model\ChannelInterface
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'findOneByHostname', array('hostname' => $hostname), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->findOneByHostname($hostname);
    }

    public function findOneByCode(string $code) : ?\Sylius\Component\Channel\Model\ChannelInterface
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'findOneByCode', array('code' => $code), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->findOneByCode($code);
    }

    public function findByName(string $name) : iterable
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'findByName', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->findByName($name);
    }

    public function add(\Sylius\Component\Resource\Model\ResourceInterface $resource) : void
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'add', array('resource' => $resource), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $this->valueHolder142bd->add($resource);
return;
    }

    public function remove(\Sylius\Component\Resource\Model\ResourceInterface $resource) : void
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'remove', array('resource' => $resource), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $this->valueHolder142bd->remove($resource);
return;
    }

    public function createPaginator(array $criteria = [], array $sorting = []) : iterable
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'createPaginator', array('criteria' => $criteria, 'sorting' => $sorting), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->createPaginator($criteria, $sorting);
    }

    public function createQueryBuilder($alias, $indexBy = null)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'createQueryBuilder', array('alias' => $alias, 'indexBy' => $indexBy), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->createQueryBuilder($alias, $indexBy);
    }

    public function createResultSetMappingBuilder($alias)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'createResultSetMappingBuilder', array('alias' => $alias), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->createResultSetMappingBuilder($alias);
    }

    public function createNamedQuery($queryName)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'createNamedQuery', array('queryName' => $queryName), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->createNamedQuery($queryName);
    }

    public function createNativeNamedQuery($queryName)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'createNativeNamedQuery', array('queryName' => $queryName), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->createNativeNamedQuery($queryName);
    }

    public function clear()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'clear', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->clear();
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'find', array('id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->find($id, $lockMode, $lockVersion);
    }

    public function findAll()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'findAll', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'findBy', array('criteria' => $criteria, 'orderBy' => $orderBy, 'limit' => $limit, 'offset' => $offset), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'findOneBy', array('criteria' => $criteria, 'orderBy' => $orderBy), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->findOneBy($criteria, $orderBy);
    }

    public function count(array $criteria)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'count', array('criteria' => $criteria), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->count($criteria);
    }

    public function __call($method, $arguments)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__call', array('method' => $method, 'arguments' => $arguments), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->__call($method, $arguments);
    }

    public function getClassName()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getClassName', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getClassName();
    }

    public function matching(\Doctrine\Common\Collections\Criteria $criteria)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'matching', array('criteria' => $criteria), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->matching($criteria);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        unset($instance->_entityName, $instance->_em, $instance->_class);

        $instance->initializer90675 = $initializer;

        return $instance;
    }

    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
        static $reflection;

        if (! $this->valueHolder142bd) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\ChannelBundle\\Doctrine\\ORM\\ChannelRepository');
            $this->valueHolder142bd = $reflection->newInstanceWithoutConstructor();
        unset($this->_entityName, $this->_em, $this->_class);

        }

        $this->valueHolder142bd->__construct($em, $class);
    }

    public function & __get($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__get', ['name' => $name], $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        if (isset(self::$publicProperties2e9c3[$name])) {
            return $this->valueHolder142bd->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__isset', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__unset', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__clone', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $this->valueHolder142bd = clone $this->valueHolder142bd;
    }

    public function __sleep()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__sleep', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return array('valueHolder142bd');
    }

    public function __wakeup()
    {
        unset($this->_entityName, $this->_em, $this->_class);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer90675 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer90675;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'initializeProxy', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder142bd;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder142bd;
    }


}
