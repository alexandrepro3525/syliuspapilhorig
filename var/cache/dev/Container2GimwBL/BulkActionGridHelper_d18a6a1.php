<?php
include_once \dirname(__DIR__, 4).'\\vendor\\sylius\\grid-bundle\\src\\Bundle\\Templating\\Helper\\BulkActionGridHelper.php';

class BulkActionGridHelper_d18a6a1 extends \Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder142bd = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer90675 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties2e9c3 = [
        
    ];

    public function renderBulkAction(\Sylius\Component\Grid\View\GridView $gridView, \Sylius\Component\Grid\Definition\Action $bulkAction, $data = null) : string
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'renderBulkAction', array('gridView' => $gridView, 'bulkAction' => $bulkAction, 'data' => $data), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->renderBulkAction($gridView, $bulkAction, $data);
    }

    public function getName() : string
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getName', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getName();
    }

    public function setCharset($charset)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'setCharset', array('charset' => $charset), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->setCharset($charset);
    }

    public function getCharset()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'getCharset', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return $this->valueHolder142bd->getCharset();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        unset($instance->charset);

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper $instance) {
            unset($instance->bulkActionGridRenderer);
        }, $instance, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper')->__invoke($instance);

        $instance->initializer90675 = $initializer;

        return $instance;
    }

    public function __construct(\Sylius\Component\Grid\Renderer\BulkActionGridRendererInterface $bulkActionGridRenderer)
    {
        static $reflection;

        if (! $this->valueHolder142bd) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper');
            $this->valueHolder142bd = $reflection->newInstanceWithoutConstructor();
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper $instance) {
            unset($instance->bulkActionGridRenderer);
        }, $this, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper')->__invoke($this);

        }

        $this->valueHolder142bd->__construct($bulkActionGridRenderer);
    }

    public function & __get($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__get', ['name' => $name], $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        if (isset(self::$publicProperties2e9c3[$name])) {
            return $this->valueHolder142bd->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__isset', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__unset', array('name' => $name), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder142bd;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder142bd;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__clone', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        $this->valueHolder142bd = clone $this->valueHolder142bd;
    }

    public function __sleep()
    {
        $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, '__sleep', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;

        return array('valueHolder142bd');
    }

    public function __wakeup()
    {
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper $instance) {
            unset($instance->bulkActionGridRenderer);
        }, $this, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer90675 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializer90675;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer90675 && ($this->initializer90675->__invoke($valueHolder142bd, $this, 'initializeProxy', array(), $this->initializer90675) || 1) && $this->valueHolder142bd = $valueHolder142bd;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder142bd;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder142bd;
    }


}
