<?php
include_once \dirname(__DIR__, 4).'\\vendor\\friendsofsymfony\\oauth2-php\\lib\\OAuth2.php';

class OAuth2_5885b82 extends \OAuth2\OAuth2 implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder78867 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera0131 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesd6273 = [
        
    ];

    public function getVariable($name, $default = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getVariable', array('name' => $name, 'default' => $default), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getVariable($name, $default);
    }

    public function setVariable($name, $value)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'setVariable', array('name' => $name, 'value' => $value), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->setVariable($name, $value);
    }

    public function verifyAccessToken($tokenParam, $scope = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'verifyAccessToken', array('tokenParam' => $tokenParam, 'scope' => $scope), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->verifyAccessToken($tokenParam, $scope);
    }

    public function getBearerToken(?\Symfony\Component\HttpFoundation\Request $request = null, $removeFromRequest = false)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'getBearerToken', array('request' => $request, 'removeFromRequest' => $removeFromRequest), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->getBearerToken($request, $removeFromRequest);
    }

    public function grantAccessToken(?\Symfony\Component\HttpFoundation\Request $request = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'grantAccessToken', array('request' => $request), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->grantAccessToken($request);
    }

    public function finishClientAuthorization($isAuthorized, $data = null, ?\Symfony\Component\HttpFoundation\Request $request = null, $scope = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'finishClientAuthorization', array('isAuthorized' => $isAuthorized, 'data' => $data, 'request' => $request, 'scope' => $scope), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->finishClientAuthorization($isAuthorized, $data, $request, $scope);
    }

    public function createAccessToken(\OAuth2\Model\IOAuth2Client $client, $data, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'createAccessToken', array('client' => $client, 'data' => $data, 'scope' => $scope, 'access_token_lifetime' => $access_token_lifetime, 'issue_refresh_token' => $issue_refresh_token, 'refresh_token_lifetime' => $refresh_token_lifetime), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->createAccessToken($client, $data, $scope, $access_token_lifetime, $issue_refresh_token, $refresh_token_lifetime);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        unset($instance->conf, $instance->storage, $instance->oldRefreshToken, $instance->usedAuthCode);

        $instance->initializera0131 = $initializer;

        return $instance;
    }

    public function __construct(\OAuth2\IOAuth2Storage $storage, $config = [])
    {
        static $reflection;

        if (! $this->valueHolder78867) {
            $reflection = $reflection ?? new \ReflectionClass('OAuth2\\OAuth2');
            $this->valueHolder78867 = $reflection->newInstanceWithoutConstructor();
        unset($this->conf, $this->storage, $this->oldRefreshToken, $this->usedAuthCode);

        }

        $this->valueHolder78867->__construct($storage, $config);
    }

    public function & __get($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__get', ['name' => $name], $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        if (isset(self::$publicPropertiesd6273[$name])) {
            return $this->valueHolder78867->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__isset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__unset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__clone', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $this->valueHolder78867 = clone $this->valueHolder78867;
    }

    public function __sleep()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__sleep', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return array('valueHolder78867');
    }

    public function __wakeup()
    {
        unset($this->conf, $this->storage, $this->oldRefreshToken, $this->usedAuthCode);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializera0131 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializera0131;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'initializeProxy', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder78867;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder78867;
    }


}
