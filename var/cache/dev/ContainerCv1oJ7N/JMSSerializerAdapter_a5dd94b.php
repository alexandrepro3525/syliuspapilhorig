<?php
include_once \dirname(__DIR__, 4).'\\vendor\\friendsofsymfony\\rest-bundle\\Serializer\\Serializer.php';
include_once \dirname(__DIR__, 4).'\\vendor\\friendsofsymfony\\rest-bundle\\Serializer\\JMSSerializerAdapter.php';

class JMSSerializerAdapter_a5dd94b extends \FOS\RestBundle\Serializer\JMSSerializerAdapter implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder78867 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera0131 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesd6273 = [
        
    ];

    public function serialize($data, $format, \FOS\RestBundle\Context\Context $context)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'serialize', array('data' => $data, 'format' => $format, 'context' => $context), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->serialize($data, $format, $context);
    }

    public function deserialize($data, $type, $format, \FOS\RestBundle\Context\Context $context)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'deserialize', array('data' => $data, 'type' => $type, 'format' => $format, 'context' => $context), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return $this->valueHolder78867->deserialize($data, $type, $format, $context);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\FOS\RestBundle\Serializer\JMSSerializerAdapter $instance) {
            unset($instance->serializer, $instance->serializationContextFactory, $instance->deserializationContextFactory);
        }, $instance, 'FOS\\RestBundle\\Serializer\\JMSSerializerAdapter')->__invoke($instance);

        $instance->initializera0131 = $initializer;

        return $instance;
    }

    public function __construct(\JMS\Serializer\SerializerInterface $serializer, ?\JMS\Serializer\ContextFactory\SerializationContextFactoryInterface $serializationContextFactory = null, ?\JMS\Serializer\ContextFactory\DeserializationContextFactoryInterface $deserializationContextFactory = null)
    {
        static $reflection;

        if (! $this->valueHolder78867) {
            $reflection = $reflection ?? new \ReflectionClass('FOS\\RestBundle\\Serializer\\JMSSerializerAdapter');
            $this->valueHolder78867 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\FOS\RestBundle\Serializer\JMSSerializerAdapter $instance) {
            unset($instance->serializer, $instance->serializationContextFactory, $instance->deserializationContextFactory);
        }, $this, 'FOS\\RestBundle\\Serializer\\JMSSerializerAdapter')->__invoke($this);

        }

        $this->valueHolder78867->__construct($serializer, $serializationContextFactory, $deserializationContextFactory);
    }

    public function & __get($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__get', ['name' => $name], $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        if (isset(self::$publicPropertiesd6273[$name])) {
            return $this->valueHolder78867->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__isset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__unset', array('name' => $name), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder78867;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder78867;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__clone', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        $this->valueHolder78867 = clone $this->valueHolder78867;
    }

    public function __sleep()
    {
        $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, '__sleep', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;

        return array('valueHolder78867');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\FOS\RestBundle\Serializer\JMSSerializerAdapter $instance) {
            unset($instance->serializer, $instance->serializationContextFactory, $instance->deserializationContextFactory);
        }, $this, 'FOS\\RestBundle\\Serializer\\JMSSerializerAdapter')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializera0131 = $initializer;
    }

    public function getProxyInitializer()
    {
        return $this->initializera0131;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera0131 && ($this->initializera0131->__invoke($valueHolder78867, $this, 'initializeProxy', array(), $this->initializera0131) || 1) && $this->valueHolder78867 = $valueHolder78867;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder78867;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder78867;
    }


}
