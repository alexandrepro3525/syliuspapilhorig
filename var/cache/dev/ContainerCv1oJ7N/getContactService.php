<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.errored..service_locator.3d8DxA1.App\Entity\Contact' shared service.

include_once \dirname(__DIR__, 4).'\\src\\Entity\\Contact.php';

return $this->privates['.errored..service_locator.3d8DxA1.App\\Entity\\Contact'] = new \App\Entity\Contact();
