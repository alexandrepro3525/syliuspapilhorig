<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'ewz_recaptcha.validator.true' shared service.

include_once \dirname(__DIR__, 4).'\\vendor\\symfony\\validator\\ConstraintValidatorInterface.php';
include_once \dirname(__DIR__, 4).'\\vendor\\symfony\\validator\\ConstraintValidator.php';
include_once \dirname(__DIR__, 4).'\\vendor\\excelwebzone\\recaptcha-bundle\\src\\Validator\\Constraints\\IsTrueValidator.php';

return $this->services['ewz_recaptcha.validator.true'] = new \EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrueValidator(false, $this->getEnv('EWZ_RECAPTCHA_SECRET'), ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), $this->parameters['ewz_recaptcha.http_proxy'], false, ($this->services['security.authorization_checker'] ?? $this->getSecurity_AuthorizationCheckerService()), [], 'www.google.com', NULL);
